<?php
basename($_SERVER['PHP_SELF']) == basename(__FILE__) && exit;
if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);

if (file_exists('legifrance_acco/' . $_GET['siren'] . '.json'))
{
	if (time() > filemtime('legifrance_acco/' . $_GET['siren'] . '.json') + 86400)
	{
		unlink('legifrance_acco/' . $_GET['siren'] . '.json');
		unlink('legifrance_acco/' . $_GET['siren'] . '_brut.json');
	}
	else
		$etablissements_avec_accords = file_get_contents('legifrance_acco/' . $_GET['siren'] . '.json');
}

if (!$etablissements_avec_accords)
{
	//LOGIN
	curl_setopt($curl, CURLOPT_URL, "https://oauth.aife.economie.gouv.fr/api/oauth/token");
	curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=" . $piste_client_id . "&client_secret=" . $piste_secret . "&scope=openid");
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept-Encoding: gzip,deflate",
	"Content-Type: application/x-www-form-urlencoded",
	"Content-Length: 140",
	"Host: oauth.aife.economie.gouv.fr",
	"Connection: Keep-Alive",
	"User-Agent: Apache-HttpClient/4.1.1 (java 1.5)"
	));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, 0);
	$result = curl_exec($curl);
	$access_token = json_decode($result);
	$access_token = $access_token->access_token;


	//RECHERCHE DES ETABLISSEMENTS QUI ONT UN ACCORD
	$params = '{"searchText":"' . $_GET['siren'] . '"}';
	curl_setopt($curl, CURLOPT_URL, "https://api.aife.economie.gouv.fr/dila/legifrance-beta/lf-engine-app/suggest/acco");
	curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	"Authorization: Bearer ". $access_token,
	"Accept-Encoding: gzip,deflate",
	"Content-Type: application/json",
	//"Host: oauth.aife.economie.gouv.fr",
	"Content-Length: " . strlen($params),
	"Connection: Keep-Alive",
	"User-Agent: Apache-HttpClient/4.1.1 (java 1.5)",
	));
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_HEADER, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($curl);
	$result = json_decode($result);
	foreach ($result->results->siret_raison_sociale as $etablissement_avec_accord)
		$etablissement_avec_accords[$etablissement_avec_accord->siret] = array();

	foreach($etablissement_avec_accords as $key => $value)
	{

		$params = '{
		"recherche": 
		{
			"champs": 
			[
				{
					"typeChamp": "RAISON_SOCIALE",
					"criteres": 
					[
						{
							"typeRecherche": "EXACTE",
							"valeur": "' . $unitelegale['Dénomination Sociale'] . '",
							"operateur": "ET"
						}
					],
					"operateur": "ET"
				}
			],
			"filtres":
			[
					{
						"facette": "SIRET_RAISON_SOCIALE",
						"valeurs": ["' . $key . '"]
					}
			],
			"pageNumber": 1,
			"pageSize": 10,
			"operateur": "OU",
			"sort": "DATE_DESC",
			"typePagination": "DEFAUT"
		},
		"fond": "ACCO"
		}';
		curl_setopt($curl, CURLOPT_URL, "https://api.aife.economie.gouv.fr/dila/legifrance-beta/lf-engine-app/search");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		"Authorization: Bearer ". $access_token,
		"Accept-Encoding: gzip,deflate",
		"Content-Type: application/json",
		"Content-Length: " . strlen($params),
		//"Host: sandbox-oauth.aife.economie.gouv.fr",
		"Connection: Keep-Alive",
		"User-Agent: Apache-HttpClient/4.1.1 (java 1.5)",
		));
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($curl);
		$result = json_decode($result);
		
		foreach($result->results as $accord)
		{
			$etablissements_avec_accords[$key]['accords'][] = array
			(
				"titre" => $accord->titles[0]->title,
				"id" => $accord->titles[0]->id,
				"raison_sociale" => $accord->raisonSociale,
				"signature" => date('Y-m-d',$accord->dateSignature/1000),
				"diffusion" => date('Y-m-d',$accord->dateDiffusion/1000),
				"themes" => $accord->themes
			);
			$etablissements_avec_accords_brut[$key]['accords'][] = $accord;
		}
	}
	
	file_put_contents('legifrance_acco/' . $_GET['siren'] . '.json', json_encode($etablissements_avec_accords));
	file_put_contents('legifrance_acco/' . $_GET['siren'] . '_brut.json', json_encode($etablissements_avec_accords_brut));
}
else
	$etablissements_avec_accords = json_decode($etablissements_avec_accords,TRUE);
?>
