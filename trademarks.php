<?php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	$curl = curl_init();
	
curl_setopt($curl, CURLOPT_URL, "https://data.inpi.fr/search");
curl_setopt($curl, CURLOPT_POSTFIELDS, '{"query":{"type":"brands","selectedIds":[],"sort":"relevance","order":"asc","nbResultsPerPage":"1000","page":"1","filter":{},"q":"' . $_GET['siren'] . '","displayStyle":"List"},"aggregations":["markCurrentStatusCode","markFeature","registrationOfficeCode","classDescriptionDetails.class"]}');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);

$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
if ($http_status>=500)
	$errors[] = "ERREUR " . $http_status . " <br/>L'API \"Marques\" est momentanément inaccessible<br/>Veuillez réessayer ultérieurement";

if ($_GET['format'] == 'json')
	die(print_r($result));
$result = json_decode($result);

foreach ($result->result->hits->hits as $marque)
{
	$marque = $marque->_source;
	
	$classes = array();
	foreach($marque->classDescriptionDetails as $classe)
		if($classe->class != '00')
		$classes[] = $classe->class;
	$classes = implode(', ', $classes);
	
	$marques[$marque->applicationDate.'_'.$marque->applicationNumber] = array
	(
		"numero" => $marque->applicationNumber,
		"office" => $marque->registrationOfficeCode,
		"nom" => $marque->markWordElement,
		"depot" => date('d/m/Y',$marque->applicationDate/1000),
		"expiration" => date('d/m/Y',$marque->expiryDate/1000),
		"type" => $marque->markFeature,
		"classes" => $classes,
	);
}
?>
