<?php
header("X-Robots-Tag: noindex, nofollow", true);

if ($_GET['id_fichier'] && !preg_match('/^[0-9]+$/',$_GET['id_fichier'])) die ('Invalid id_fichier : ' . $_GET['id_fichier']);

include('config.php');

$curl = curl_init();

if (time() > filemtime('rbe_token.txt') + 1700)
{
	curl_setopt($curl, CURLOPT_URL, 'https://opendata-rncs.inpi.fr/services/diffusion/login');
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('login: ' . $rbe_login, 'password: ' . $rbe_password));
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HEADER, true);
	$result = curl_exec($curl);

	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if ($http_status!=200)
		die("ERREUR " . $http_status . " <br/>L'API RBE est momentanément indisponible<br/>Veuillez réessayer ultérieurement");

	$jsessionid = substr($result,strpos($result,'JSESSIONID=')+11,32);
	file_put_contents('rbe_token.txt',$jsessionid);
}
else
	$jsessionid = file_get_contents('rbe_token.txt');

curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/actes-rbe/get?idFichier=" . $_GET['id_fichier']);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: JSESSIONID='.$jsessionid));
curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
curl_setopt($curl, CURLOPT_POST, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$zip = curl_exec($curl);

$filesectors = explode("\x50\x4b\x01\x02", $zip);
$filesectors = explode("\x50\x4b\x03\x04", $filesectors[0]);
array_shift($filesectors);
foreach($filesectors as $filesector)
{
	$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $filesector);
	$filedescription['filename'] = substr($filesector,26,$filedescription['filename_length']);
	if (substr($filedescription['filename'],-3) == 'pdf')
		$file = gzinflate(substr($filesector,26+$filedescription['filename_length'],-12));
}

curl_close($curl);

if (sizeof($file)==0 && $http_status==200)
	die("<html><body style=\"font:normal 14px Consolas\"><p>Ce fichier n'a pas pu être trouvé dans la base publique<br/>Certains dépôts récents peuvent ne pas encore avoir été mis en ligne</p></body></html>");

if ($_GET['method']=='inline')
{
	header('Content-type: application/pdf');
	header('Content-Disposition: inline; filename="' . $_GET['id_fichier'] . '.pdf');
}
else
{
	header('Content-Description: File Transfer');
	header('Content-Type: application/force-download');
	header('Content-Length: ' . strlen($file));
	header('Content-Disposition: attachment; filename="' . $_GET['id_fichier'] . '.pdf');
}

echo $file;
?>
