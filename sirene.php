<?php
basename($_SERVER['PHP_SELF']) == basename(__FILE__) && exit;
if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);

if (file_exists('sirene/' . $_GET['siren'] . '_etablissements.json'))
{
	if (time() > filemtime('sirene/' . $_GET['siren'] . '_etablissements.json') + 86400)
	{
		unlink('sirene/' . $_GET['siren'] . '_etablissements.json');
		unlink('sirene/' . $_GET['siren'] . '_unitelegale.json');
	}
	else
	{
		$json_etablissements = file_get_contents('sirene/' . $_GET['siren'] . '_etablissements.json');
		$json_unitelegale = file_get_contents('sirene/' . $_GET['siren'] . '_unitelegale.json');
	}
}

//ETABLISSEMENTS
if (!$json_etablissements)
{	
	curl_setopt($curl, CURLOPT_URL, "https://api.insee.fr/token");
	curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic " . $sirene_auth_request_key));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($curl);
	$access_token = json_decode($result);
	$access_token = $access_token->access_token;

	curl_setopt($curl, CURLOPT_URL, "https://api.insee.fr/entreprises/sirene/V3/siret?q=siren:" . $_GET['siren'] . "&nombre=1000");
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
	curl_setopt($curl, CURLOPT_POST, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$json = curl_exec($curl);
	$json_etablissements = json_decode($json);
	
	if (!$json_etablissements->header->statut)
		echo '<br/><span style="color:#FF0000">API SIRENE indisponible<br/>Veuillez réessayer dans quelques minutes</span>';
	else if ($json_etablissements->header->statut == 200)
		file_put_contents('sirene/' . $_GET['siren'] . '_etablissements.json',$json);
	else if ($json_etablissements->header->statut == 404)
		echo "<br/>Aucun résultat<br/>";
	else
		echo '<br/>Erreur : Code retour ' . $json_etablissements->header->statut . '<br/>';
}
else
	$json_etablissements = json_decode($json_etablissements);


//UNITE LEGALE
if (!$json_unitelegale)
{	
	curl_setopt($curl, CURLOPT_URL, "https://api.insee.fr/entreprises/sirene/V3/siren/" . $_GET['siren']);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
	curl_setopt($curl, CURLOPT_POST, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$json = curl_exec($curl);
	$json_unitelegale = json_decode($json);
	
	if (!$json_unitelegale->header->statut)
		echo '<br/><span style="color:#FF0000">API SIRENE indisponible<br/>Veuillez réessayer dans quelques minutes</span>';
	else if ($json_unitelegale->header->statut == 200)
		file_put_contents('sirene/' . $_GET['siren'] . '_unitelegale.json',$json);
	else if ($json_unitelegale->header->statut == 404)
		echo "<br/>Aucun résultat<br/>";
	else
		echo '<br/>Erreur : Code retour ' . $json_unitelegale->header->statut . '<br/>';
}
else
	$json_unitelegale = json_decode($json_unitelegale);



if ($json_etablissements->header->nombre)
{
	for ($x=0; $x<$json_etablissements->header->nombre; $x++)
	{
		$etablissements[$json_etablissements->etablissements[$x]->siret]['siret'] = $json_etablissements->etablissements[$x]->siret;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['creation'] = $json_etablissements->etablissements[$x]->dateCreationEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['siege'] = $json_etablissements->etablissements[$x]->etablissementSiege;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['enseigne'][] = $json_etablissements->etablissements[$x]->periodesEtablissement[0]->enseigne1Etablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['enseigne'][] = $json_etablissements->etablissements[$x]->periodesEtablissement[0]->enseigne2Etablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['enseigne'][] = $json_etablissements->etablissements[$x]->periodesEtablissement[0]->enseigne3Etablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['enseigne'][] = $json_etablissements->etablissements[$x]->periodesEtablissement[0]->denominationUsuelleEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['ape'] = $json_etablissements->etablissements[$x]->periodesEtablissement[0]->activitePrincipaleEtablissement;
		if ($json_etablissements->etablissements[$x]->periodesEtablissement[0]->etatAdministratifEtablissement == "F")
			$etablissements[$json_etablissements->etablissements[$x]->siret]['fermeture'] = $json_etablissements->etablissements[$x]->periodesEtablissement[0]->dateDebut;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['complementAdresse'] = $json_etablissements->etablissements[$x]->adresseEtablissement->complementAdresseEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['numeroVoie'] = $json_etablissements->etablissements[$x]->adresseEtablissement->numeroVoieEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['indiceRepetition'] = $json_etablissements->etablissements[$x]->adresseEtablissement->indiceRepetitionEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['typeVoie'] = $json_etablissements->etablissements[$x]->adresseEtablissement->typeVoieEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libelleVoie'] = $json_etablissements->etablissements[$x]->adresseEtablissement->libelleVoieEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['codePostal'] = $json_etablissements->etablissements[$x]->adresseEtablissement->codePostalEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libelleCommune'] = $json_etablissements->etablissements[$x]->adresseEtablissement->libelleCommuneEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libelleCommuneEtranger'] = $json_etablissements->etablissements[$x]->adresseEtablissement->libelleCommuneEtrangerEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['distribution'] = $json_etablissements->etablissements[$x]->adresseEtablissement->distributionSpecialeEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['codeCommune'] = $json_etablissements->etablissements[$x]->adresseEtablissement->codeCommuneEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['codeCedex'] = $json_etablissements->etablissements[$x]->adresseEtablissement->codeCedexEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libelleCedex'] = $json_etablissements->etablissements[$x]->adresseEtablissement->libelleCedexEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['codePaysEtranger'] = $json_etablissements->etablissements[$x]->adresseEtablissement->codePaysEtrangerEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libellePaysEtranger'] = $json_etablissements->etablissements[$x]->adresseEtablissement->libellePaysEtrangerEtablissement;
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse'][] = $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['numeroVoie'] . $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['indiceRepetition'] . ' ' . strtoupper($type_voie[$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['typeVoie']]) . ' ' . $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libelleVoie'];
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse'][] = $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['complementAdresse'];
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse'][] = $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['distribution'];
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse'][] = $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['codeCedex'] . ' ' . $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libelleCedex'];
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse'][] = $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['codePostal'] . ' ' . $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libelleCommune'] . $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libelleCommuneEtranger'];
		$etablissements[$json_etablissements->etablissements[$x]->siret]['adresse'][] = $etablissements[$json_etablissements->etablissements[$x]->siret]['adresse_detail']['libellePaysEtranger'];
	}

	

	//UNITE LEGALE
	if ($json_unitelegale->uniteLegale->periodesUniteLegale[0]->denominationUniteLegale)
	{
		if (!$unitelegale['Dénomination Sociale'])
			$unitelegale['Dénomination Sociale'] = $json_unitelegale->uniteLegale->periodesUniteLegale[0]->denominationUniteLegale;
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[0]->denominationUsuelle1UniteLegale)
			$unitelegale['Nom Commercial'][] = $json_unitelegale->uniteLegale->periodesUniteLegale[0]->denominationUsuelle1UniteLegale;
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[0]->denominationUsuelle2UniteLegale)
			$unitelegale['Nom Commercial'][] = $json_unitelegale->uniteLegale->periodesUniteLegale[0]->denominationUsuelle2UniteLegale;
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[0]->denominationUsuelle3UniteLegale)
			$unitelegale['Nom Commercial'][] = $json_unitelegale->uniteLegale->periodesUniteLegale[0]->denominationUsuelle3UniteLegale;
	}
	else
		$unitelegale['Nom'] = ucfirst(strtolower($json_unitelegale->uniteLegale->prenom1UniteLegale)) . ' ' . ucfirst(strtolower($json_unitelegale->uniteLegale->prenom2UniteLegale)) . ' ' . ucfirst(strtolower($json_unitelegale->uniteLegale->prenom3UniteLegale)) . ' ' . ucfirst(strtolower($json_unitelegale->uniteLegale->prenom4UniteLegale)) . ' ' . $json_unitelegale->uniteLegale->periodesUniteLegale[0]->nomUniteLegale . " " . $json_unitelegale->uniteLegale->periodesUniteLegale[0]->nomUsageUniteLegale;
	foreach($etablissements as $etablissement)
		if ($etablissement['siege']==1)
			$unitelegale['Siège Social'] = implode('<br/>',array_filter(array_map('trim', $etablissement['adresse'])));
	$unitelegale['N° SIREN'] = $json_unitelegale->uniteLegale->siren;
	$unitelegale['Forme Juridique'] = $categorie_juridique[$json_unitelegale->uniteLegale->periodesUniteLegale[0]->categorieJuridiqueUniteLegale];
	$unitelegale['Immatriculation'] = date('d/m/Y',strtotime($json_unitelegale->uniteLegale->dateCreationUniteLegale));
	$unitelegale['Code NAF'] = $json_unitelegale->uniteLegale->periodesUniteLegale[0]->activitePrincipaleUniteLegale;
	$unitelegale['Activité'] = $naf[$json_unitelegale->uniteLegale->periodesUniteLegale[0]->activitePrincipaleUniteLegale];
	$unitelegale['Effectif salarié'] = $effectif_salarie[$json_unitelegale->uniteLegale->trancheEffectifsUniteLegale];
	$unitelegale['TVA Intracommunautaire'] = 'FR' . (12 + 3 * ($json_unitelegale->uniteLegale->siren % 97) % 97) . $json_unitelegale->uniteLegale->siren;
	for ($x = ($json_unitelegale->uniteLegale->nombrePeriodesUniteLegale-1); $x >= 1; $x--)
	{
		if ($x == ($json_unitelegale->uniteLegale->nombrePeriodesUniteLegale-1))
		{
			$historique_unitelegale[0]['date'] = $json_unitelegale->uniteLegale->periodesUniteLegale[$x]->dateDebut;
			$historique_unitelegale[0]['description'] = "Création &rarr; ";
			if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x]->denominationUniteLegale)
				$historique_unitelegale[0]['description'] .= $json_unitelegale->uniteLegale->periodesUniteLegale[$x]->denominationUniteLegale;
			else
				$historique_unitelegale[0]['description'] .= $json_unitelegale->uniteLegale->prenom1UniteLegale . ' ' . $json_unitelegale->uniteLegale->prenom2UniteLegale . ' ' . $json_unitelegale->uniteLegale->prenom3UniteLegale . ' ' . $json_unitelegale->uniteLegale->prenom4UniteLegale . ' ' . $json_unitelegale->uniteLegale->periodesUniteLegale[$x]->nomUniteLegale . " " . $json_unitelegale->uniteLegale->periodesUniteLegale[$x]->nomUsageUniteLegale;
			$historique_unitelegale[0]['description'] .= ' ' . $categorie_juridique[$json_unitelegale->uniteLegale->periodesUniteLegale[$x]->categorieJuridiqueUniteLegale];
		}
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementEconomieSocialeSolidaireUniteLegale == 1)
		{
			if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x]->economieSocialeSolidaireUniteLegale AND $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->economieSocialeSolidaireUniteLegale == 'O')
				$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de catégorie &rarr; économie sociale");
			else if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x]->economieSocialeSolidaireUniteLegale AND $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->economieSocialeSolidaireUniteLegale == 'N')
				$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de catégorie &rarr; économie non sociale");
		}
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementCaractereEmployeurUniteLegale == 1)
		{
			if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x]->caractereEmployeurUniteLegale AND $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->caractereEmployeurUniteLegale == 'O')
				$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de catégorie &rarr; Employeur");
			else if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x]->caractereEmployeurUniteLegale AND $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->caractereEmployeurUniteLegale == 'N')
				$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de catégorie &rarr; Non Employeur");
		}
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementNicSiegeUniteLegale == 1)
			$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Transfert de siège social &rarr; " . implode(' ',$etablissements[$_GET['siren'].$json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->nicSiegeUniteLegale]['adresse']));
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementActivitePrincipaleUniteLegale == 1)
			$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement d'activité &rarr; " . $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->activitePrincipaleUniteLegale . ' (' . $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->nomenclatureActivitePrincipaleUniteLegale . ')');
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementCategorieJuridiqueUniteLegale == 1)
			$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de forme &rarr; " . $categorie_juridique[$json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->categorieJuridiqueUniteLegale]);
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementDenominationUsuelleUniteLegale == 1)
			$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de dénomination usuelle &rarr; " . $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->denominationUsuelle1UniteLegale);
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementDenominationUniteLegale == 1)
			$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de dénomination sociale &rarr; " . $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->denominationUniteLegale);
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementNomUsageUniteLegale == 1)
			$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de nom d'usage &rarr; " . $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->nomUsageUniteLegale);
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementNomUniteLegale == 1)
			$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Changement de nom de naissance &rarr; " . $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->nomUniteLegale);
		if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->changementEtatAdministratifUniteLegale == 1)
		{
			if ($json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->etatAdministratifUniteLegale == 'A')
				$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Réouverture de la société");
			else
				$historique_unitelegale[] = array("date" => $json_unitelegale->uniteLegale->periodesUniteLegale[$x-1]->dateDebut, "description" => "Fermeture ou mise en sommeil de la société");
		}
	}
	rsort($historique_unitelegale);
}
?>
