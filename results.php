<?php
$_GET['type'] = preg_replace('/[^0-9]/', '', $_GET['type']);
$_GET['denomination'] = preg_replace('/[^0-9a-zA-Zàâäéèêëïîôöùûüÿç@&?!\'.\/*%\-\s]/', '', urldecode($_GET['denomination']));
$_GET['numero_voie'] = preg_replace('/[^0-9a-zA-Z\s]/', '', $_GET['numero_voie']);
$_GET['rue'] = preg_replace('/[^0-9a-zA-Zéàèêû\'\s]/', '', urldecode($_GET['rue']));
$_GET['commune'] = preg_replace('/[^0-9a-zA-Zàâäéèêëïîôöùûüÿç\-\s]/', '', urldecode($_GET['commune']));
$_GET['code_postal'] = preg_replace('/[^0-9]/', '', $_GET['code_postal']);
$_GET['naf'] = preg_replace('/[^0-9a-zA-Z.]/', '', $_GET['naf']);

if (is_numeric(str_replace(' ','',$_GET['denomination'])) AND strlen(str_replace(' ','',$_GET['denomination'])) >= 9)
	$_GET['denomination'] = str_replace(' ','',$_GET['denomination']);

include('index.php');
include('constants.php');

if ($_GET['denomination'])
{
	if ($_GET['approximation'])
		$query_parameters[] = 'raisonSociale:' . preg_replace('/\s+/', '',$_GET['denomination']) . '~2 AND -raisonSociale:"' . $_GET['denomination'] . '"';
	else
	{
		$query_parameters[] = '(raisonSociale:"' . $_GET['denomination'] . '" OR (prenom1UniteLegale:"' . explode(' ',$_GET['denomination'])[0] . '" AND nomUniteLegale:"' . explode(' ',$_GET['denomination'])[1] . '") OR (prenom1UniteLegale:"' . explode(' ',$_GET['denomination'])[1] . '" AND nomUniteLegale:"' . explode(' ',$_GET['denomination'])[0] . '"))';
		echo '<br/><br/><a href="?denomination=' . $_GET['denomination'] . '&approximation=1">Effectuer une recherche par approximation</a><br/>';
	}
}
if (!$_GET['type'] OR $_GET['type'] == 1)
	$query_parameters[] = 'etablissementSiege:1';
else if ($_GET['type'] == 2)
	$query_parameters[] = 'etablissementSiege:0';
	
if ($_GET['numero_voie'])
	$query_parameters[] = '(numeroVoieEtablissement:"' . $_GET['numero_voie'] . '" AND indiceRepetitionEtablissement:"' . $_GET['numero_voie'] . '")';
if ($_GET['rue'])
	$query_parameters[] = 'libelleVoieEtablissement:"' .  $_GET['rue'] . '"';
if ($_GET['code_postal'])
	$query_parameters[] = 'codePostalEtablissement:' . $_GET['code_postal'] . '*';
if ($_GET['commune'])
	$query_parameters[] = 'libelleCommuneEtablissement:"' .  $_GET['commune'] . '"';
if ($_GET['naf'])
	$query_parameters[] = 'activitePrincipaleUniteLegale:' . $_GET['naf'];
$query .= 'q=' . implode(' AND ',$query_parameters) . $end_query;

if ($_GET['approximation'])
	$query .= '&nombre=1000';
else
	$query .='&nombre=10&debut=' . $_GET['debut'] . '&tri=denominationUniteLegale asc, nomUniteLegale asc';
$query .='&masquerValeursNulles=true';
$query .='&champs=siren,etablissementSiege,activitePrincipaleUniteLegale,prenom1UniteLegale,nomUniteLegale,denominationUniteLegale,denominationUsuelle1UniteLegale,denominationUsuelle2UniteLegale,denominationUsuelle3UniteLegale,sigleUniteLegale,complementAdresseEtablissement,numeroVoieEtablissement,indiceRepetitionEtablissement,typeVoieEtablissement,libelleVoieEtablissement,codePostalEtablissement,libelleCommuneEtablissement,libelleCommuneEtrangerEtablissement,distributionSpecialeEtablissement,codeCedexEtablissement,libelleCedexEtablissement,libellePaysEtrangerEtablissement';

$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, "https://api.insee.fr/token");
curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic " . $sirene_auth_request_key));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
$access_token = json_decode($result);
$access_token = $access_token->access_token;

sleep(0.2);

curl_setopt($curl, CURLOPT_URL, 'https://api.insee.fr/entreprises/sirene/V3/siret');
curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json','Authorization: Bearer '. $access_token));
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
$result = json_decode($result);

curl_close($curl);

if (!$result->header->statut)
	die('<br/><br/><span style="color:#FF0000">API SIRENE indisponible<br/>Veuillez réessayer dans quelques minutes</span>');
else if ($result->header->statut == 404)
	die('<br/><br/>Aucun résultat<br/>');
else if ($result->header->statut != 200)
	die('<br/><br/>Erreur : Code retour ' . $result->header->statut . '<br/>');


foreach($result->etablissements as $etablissement)
{
	$resultats[] = array
	(
		'siren' => $etablissement->siren,
		'denomination' => array
		(
			$etablissement->uniteLegale->denominationUniteLegale,
			ucfirst(strtolower($etablissement->uniteLegale->prenom1UniteLegale)) . ' ' . ucfirst(strtolower($etablissement->uniteLegale->prenom2UniteLegale)) . ' ' . ucfirst(strtolower($etablissement->uniteLegale->prenom3UniteLegale)) . ' ' . ucfirst(strtolower($etablissement->uniteLegale->prenom4UniteLegale)) . ' ' . $etablissement->uniteLegale->nomUniteLegale,
			$etablissement->uniteLegale->denominationUsuelle1UniteLegale,
			$etablissement->uniteLegale->denominationUsuelle2UniteLegale,
			$etablissement->uniteLegale->denominationUsuelle3UniteLegale,
			$etablissement->uniteLegale->sigleUniteLegale,
		),
		'naf' => $etablissement->uniteLegale->activitePrincipaleUniteLegale,
		'adresse' => array
		(
			$etablissement->adresseEtablissement->complementAdresseEtablissement,
			$etablissement->adresseEtablissement->numeroVoieEtablissement.' '.$etablissement->adresseEtablissement->indiceRepetitionEtablissement.' '.$etablissement->adresseEtablissement->typeVoieEtablissement.' '.$etablissement->adresseEtablissement->libelleVoieEtablissement,
			$etablissement->adresseEtablissement->codePostalEtablissement.' '.$etablissement->adresseEtablissement->libelleCommuneEtablissement.$etablissement->adresseEtablissement->libelleCommuneEtrangerEtablissement,
			$etablissement->adresseEtablissement->distributionSpecialeEtablissement,
			$etablissement->adresseEtablissement->codeCedexEtablissement.' '.$etablissement->adresseEtablissement->libelleCedexEtablissement,
			$etablissement->adresseEtablissement->libellePaysEtrangerEtablissement,
		)
	);
}

echo '<table class="responsive_table resultats">';
	echo '<thead>';
		echo '<tr><td colspan="4">' . $result->header->total . ' RESULTATS</td></tr>';
	echo '</thead>';
	echo '<tbody>';
	foreach($resultats as $siren => $resultat)
	{
		echo '<tr style="cursor:pointer" onclick="window.location.href = \'data.php?siren=' . $resultat['siren'] . '\'" oncontextmenu="window.open(\'data.php?siren=' . $resultat['siren'] . '\');return false">';
			echo '<td style="max-width:500px">' . implode('<br/>',array_filter(array_map('trim', $resultat['denomination']))) . '</td>';
			echo '<td>' . $resultat['siren'] . '</td>';
			echo '<td style="max-width:500px">' . implode('<br/>',array_filter(array_map('trim', $resultat['adresse']))) . '</td>';
			echo '<td style="max-width:500px">' . $resultat['naf'] . '<br/>' . $naf[$resultat['naf']] . '</td>';
		echo '</tr>';
	}
	echo '</tbody>';
	if (!$_GET['approximation'])
	{
		echo '<tfoot>';
			echo '<tr>';
				echo '<td colspan="4" style="text-align:center">';
				echo '<div style="text-align:left">' . ($result->header->debut - sizeof($resultats) >= 0?'<a href="?' . str_replace('&debut='.$_GET['debut'],'',$_SERVER['QUERY_STRING']) . '&debut=' . ($result->header->debut - sizeof($resultats)) . '">&#9668; Précédent</a>':'') . '</div>&nbsp;&nbsp;';
				echo '<div style="text-align:center">' . ($result->header->debut + 1) . ' à ' . ($result->header->debut + sizeof($resultats)) . ' sur ' . $result->header->total . '</div>';
				echo '<div style="text-align:right">' .($result->header->total > $result->header->debut + sizeof($resultats)?'<a href="?' . str_replace('&debut='.$_GET['debut'],'',$_SERVER['QUERY_STRING']) . '&debut=' . ($result->header->debut + sizeof($resultats)) . '">Suivant &#9658;</a>':'') . '</div>';
				echo '</td></tr>';
		echo '</tfoot>';
	}
echo '</table>';

echo '<div style="position:fixed;top:0;left:0"><a href="index.php"><img alt="home" class="menu_icon" style="filter:invert(1)" src="/images/home.svg"/></a></div>';

echo '<br/><br/>';
?>
