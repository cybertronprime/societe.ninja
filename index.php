<?php
$_GET['denomination'] = preg_replace('/[^0-9a-zA-Zàâäéèêëïîôöùûüÿç@&?!\'.\/*%\-\s]/', '', urldecode($_GET['denomination']));

if (is_numeric($_GET['denomination']))
	header('Location:data.php?siren=' . substr($_GET['denomination'],0,9));




if ($_GET['flush'])
{
	exec('rm -R sirene');
	exec('rm -R legifrance_acco');
	exec('rm -r bodacc');
	exec('rm -r imrs');
	exec('rm -r idcc');
	exec('mkdir sirene');
	exec('mkdir legifrance_acco');
	exec('mkdir bodacc');
	exec('mkdir imrs');
	exec('mkdir idcc');
}

include('config.php');
include('constants.php');

echo '<!DOCTYPE html>';
echo '<html lang="fr">';
echo '<head>';
	echo '<title>SOCIETE NINJA</title>';
	if (basename($_SERVER['PHP_SELF']) != 'index.php' OR $_GET['denomination'])
		echo '<meta name="robots" CONTENT="noindex">';
	echo '<meta name="robots" CONTENT="nofollow">';
	echo '<meta name="viewport" content="width=device-width, initial-scale=0.9"/>';
	echo '<meta name="description" content="Accès gratuit aux informations sur les entreprises et sociétés françaises (statuts, PV, procès verbaux, comptes annuels, bilans...)"/>';
	echo '<link rel="stylesheet" href="index.css"/>';
	echo '<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon.png"/>';
echo '</head>';
echo '<body style="overflow:auto">';

echo '<div class="title_table">';
	echo '<span class="title">SOCIETE.NINJA</span><br/>';
	echo '<span class="subtitle">Informations publiques et gratuites sur les entreprises</span>';
echo '</div>';

echo '<form id="menu_recherche" action="results.php" method="get" autocomplete="off" role="presentation">';
	echo '<div style="text-align:center">';
	if (!$_GET['denomination'])
	echo '<img alt="logo" style="width:150px;margin:20px;animation:slide-in-blurred-bottom 0.6s cubic-bezier(0.230, 1.000, 0.320, 1.000) both" src="/images/ninja.png"/>';
	echo '<br/>';
		if ($_GET['advanced']==1)
		{
			echo '<select name="type" autocomplete="off" id="type"><option value="1">SIEGE</option><option value="2"' . ($_GET['type']==2?' selected':'') . '>ETABLISSEMENTS SECONDAIRES</option><option value="3"' . ($_GET['type']==3?' selected':'') . '>SIEGE ET ETABLISSEMENTS</option></select><br/>';
			echo '<input type="text" autocomplete="off" name="denomination" id="denomination" placeholder="Dénomination" value="' . $_GET['denomination'] . '" autofocus/><br/>';
			echo '<input type="text" autocomplete="off" name="numero_voie" id="numero_voie" value="' . $_GET['numero_voie'] . '" placeholder="N° de la voie"><br/>';
			echo '<input type="text" autocomplete="off" name="rue" id="rue" value="' . $_GET['rue'] . '" placeholder="Nom de la voie (n\'indiquez pas rue, avenue...)"><br/>';
			echo '<input type="number" autocomplete="off" name="code_postal" id="code_postal" placeholder="Code Postal ou N° de département" value="' . $_GET['code_postal'] . '"><br/>';
			echo '<input type="text" autocomplete="off" name="commune" id="commune" placeholder="Commune" value="' . $_GET['commune'] . '"><br/>';
			echo '<select id="naf" name="naf" placeholder="code naf" style="font-size:13px"><option disabled selected>CODE NAF</option>';
				foreach($naf as $key => $value)
					if (strlen($key) == 6)
						echo '<option value="' . $key . '"' . ($_GET['naf']==$key?'selected':'') . '>' . $key . ' (' . $value . ')</option>';
			echo '</select>';
			echo '<input type="hidden" autocomplete="off" id="advanced" name="advanced" value="1"/>';
			echo '<br/>';
		}
		else
		{
			echo '<label for="denomination">DENOMINATION ou N° SIREN</label><br/>';
			echo '<input type="text" style="text-transform:uppercase" name="denomination" id="denomination" value="' . urldecode($_GET['denomination']) . '" required autofocus><br/><br/>';
		}
	
		echo '<input type="submit" value="Rechercher">';
	echo '</div>';
echo '</form>';

echo '<br/>';

if ($_GET['advanced'])
	echo '<a href="?' . ($_GET['denomination']?'denomination='.$_GET['denomination']:'') . '">Recherche simple</a>';
else
	echo '<a href="index.php?' . $_SERVER['QUERY_STRING'] . '&advanced=1">Recherche avancée</a>';

echo '<br/><br/><a href="dirigeant.php">Recherche par dirigeant</a>';

if (basename($_SERVER['PHP_SELF']) == 'index.php')
{
	include('stats.php');
	echo '<br/><br/><a href="mentions.php">Mentions légales</a><br/><br/>';
}
?>
