<?php
basename($_SERVER['PHP_SELF']) == basename(__FILE__) && exit;
if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);

if (file_exists('rbe/' . $_GET['siren'] . '.xml'))
{
	if (time() > filemtime('rbe/' . $_GET['siren'] . '.xml') + 86400)
	{
		unlink('rbe/' . $_GET['siren'] . '.xml');
		unlink('rbe/' . $_GET['siren'] . '_actes.json');
	}
	else
	{
		$rbexml = file_get_contents('rbe/' . $_GET['siren'] . '.xml');
		$rbeactes = file_get_contents('rbe/' . $_GET['siren'] . '_actes.json');
	}
}

if (time() > filemtime('rbe_token.txt') + 1700)
{
	curl_setopt($curl, CURLOPT_URL, 'https://opendata-rncs.inpi.fr/services/diffusion/login');
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('login: ' . $rbe_login, 'password: ' . $rbe_password));
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HEADER, true);
	$result = curl_exec($curl);

	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	$curl_errno= curl_errno($curl);
	if ($http_status==200)
	{
		$jsessionid = substr($result,strpos($result,'JSESSIONID=')+11,32);
		file_put_contents('rbe_token.txt',$jsessionid);
	}
	else
		$errors[] = "ERREUR " . $http_status . ' / ' . $curl_errno . " <br/>DATA.INPI.FR est momentanément inaccessible<br/>Les données relatives aux bénéficiaires effectifs sont en conséquence indisponibles<br/>Veuillez réessayer ultérieurement";
}
else
	$jsessionid = file_get_contents('rbe_token.txt');

if (!$rbexml)
{
	$context = stream_context_create(["http" => ["method" => "GET", "header" => "Cookie: JSESSIONID=" . $jsessionid]]);
	$result = file_get_contents("https://opendata-rncs.inpi.fr/services/diffusion/rbe-xml/get?sirens=" . $_GET['siren'] . '&mesureVigilance=aucune', false, $context);
	
	//EXTRACTION DU ZIP CONTENANT LE FICHIER XML (le fichier ZIP est traité en mémoire par décomposition de la chaîne binaire)
	//Voir la spécification du format PKZIP : https://users.cs.jmu.edu/buchhofp/forensics/formats/pkzip.html
	//On élimine le "central directory" qui commence par "\x50\x4b\x01\x02".
	//On décompose chaque fichier sachant que la signature de 4 octets commence toujours par "\x50\x4b\x03\x04"
	//Le header de chaque fichier fait 30 octets. Moins la signature, reste 26. +le nom du fichier de longueur variable précisée dans le header.
	//Le footer (data descriptor) fait quant à lui 12 octets.
	//Ce qu'il y a entre le header et le footer est donc le contenu compressé qu'on peut extraire en mémoire avec gzinflate
	//quand il y a un zip dans un zip il y a confusion entre la signature du fichier zip et celle du fichier zip dans le zip

	$filesectors = explode("\x50\x4b\x01\x02", $result);
	$filesectors = $filesectors[0] . "\x50\x4b\x01\x02" . $filesectors[1];
	$filesectors = explode("\x50\x4b\x03\x04", $filesectors);
	$zip_in_zip = $filesectors[1] . "\x50\x4b\x03\x04" . $filesectors[2];
	$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $zip_in_zip);
	$zip = gzinflate(substr($zip_in_zip,26+$filedescription['filename_length'],-12));

	$filesectors = explode("\x50\x4b\x01\x02", $zip);
	$filesectors = explode("\x50\x4b\x03\x04", $filesectors[0]);
	array_shift($filesectors);
	foreach($filesectors as $filesector)
	{
		$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $filesector);
		$filedescription['filename'] = substr($filesector,26,$filedescription['filename_length']);

		if (substr($filedescription['filename'],-3) == 'xml')
			$rbexml = gzinflate(substr($filesector,26+$filedescription['filename_length'],-12));

	}
	
	if ($rbexml AND !$errors)
		file_put_contents('rbe/' . $_GET['siren'] . '.xml',$rbexml);
	else
		file_put_contents('rbe/' . $_GET['siren'] . '.xml','null');
}

$rbe = simplexml_load_string($rbexml);

if ($rbe->dossier)
{
	$x = 0;
	foreach($rbe->dossier[0]->beneficiaires->beneficiaire as $beneficiaire)
	{
		//$beneficiaires_advanced[$x]['detail']['type_entite'] = strval($beneficiaire->type_entite);
		$beneficiaires_advanced[$x]['detail']['Bénéficiaire depuis le'] = date('d/m/Y',strtotime(strval($beneficiaire->date_benef)));
		$beneficiaires_advanced[$x]['Nom de Naissance'] = strval($beneficiaire->nom_naissance_benef);
		$beneficiaires_advanced[$x]['Nom d\'Usage'] = strval($beneficiaire->nom_usage_benef);
		$beneficiaires_advanced[$x]['Prénoms'] = strval($beneficiaire->prenoms_benef);
		$beneficiaires_advanced[$x]['Nationalité'] = strval($beneficiaire->nationalite_benef);
		$beneficiaires_advanced[$x]['Date de naissance'] =  date('d/m/Y',strtotime(strval($beneficiaire->date_naissance_benef)));
		$beneficiaires_advanced[$x]['Département de naissance'] = strval($beneficiaire->depart_naissance_benef);
		$beneficiaires_advanced[$x]['Pseudonyme'] = strval($beneficiaire->pseudo_benef);
		$beneficiaires_advanced[$x]['Ville de Naissance'] = strval($beneficiaire->ville_naissance_benef);
		$beneficiaires_advanced[$x]['Pays de Naissance'] = strval($beneficiaire->pays_naissance_benef);
		$beneficiaires_advanced[$x]['Adresse'] = strval($beneficiaire->addresse_domicile_benef);
		$beneficiaires_advanced[$x]['Code Postal'] = strval($beneficiaire->code_postal_domicile_benef);
		$beneficiaires_advanced[$x]['Ville'] = strval($beneficiaire->commune_domicile_benef);
		$beneficiaires_advanced[$x]['Pays'] = strval($beneficiaire->pays_domicile_benef);
		$beneficiaires_advanced[$x]['detail']['Capital détenu'] = strval($beneficiaire->part_totale) . ' %';
		$beneficiaires_advanced[$x]['detail']['Parts détenues directement'] = strval($beneficiaire->part_directe);
		$beneficiaires_advanced[$x]['detail']['Parts détenues en pleine propriété'] = strval($beneficiaire->part_directe_plein_prop) . ($beneficiaire->part_directe_plein_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Parts détenues en nue propriété'] = strval($beneficiaire->part_directe_nue_prop) . ($beneficiaire->part_directe_nue_prop?' %':'');
		//$beneficiaires_advanced[$x]['detail']['titu_direct_plein_prop'] = strval($beneficiaire->titu_direct_plein_prop) . ($beneficiaire->titu_direct_plein_prop?' %':'');
		//$beneficiaires_advanced[$x]['detail']['titu_direct_nue_prop'] = strval($beneficiaire->titu_direct_nue_prop) . ($beneficiaire->titu_direct_nue_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Parts détenues indirectement'] = strval($beneficiaire->part_indirecte);
		$beneficiaires_advanced[$x]['detail']['Parts détenues indirectement via une indivison'] = strval($beneficiaire->part_indirecte_ind) . ($beneficiaire->part_indirecte_ind?' %':'');
		$beneficiaires_advanced[$x]['detail']['Parts détenues indirectement via une indivision en pleine propriété'] = strval($beneficiaire->part_indirect_ind_plein_prop) . ($beneficiaire->part_indirect_ind_plein_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Parts détenues indirectement via une indivision en nue propriété'] = strval($beneficiaire->part_indirect_ind_nue_prop) . ($beneficiaire->part_indirect_ind_nue_prop?' %':'');
		//$beneficiaires_advanced[$x]['detail']['titu_indirect_ind'] = strval($beneficiaire->titu_indirect_ind);
		//$beneficiaires_advanced[$x]['detail']['titu_indirect_plein_prop'] = strval($beneficiaire->titu_indirect_plein_prop);
		//$beneficiaires_advanced[$x]['detail']['titu_indirect_nue_prop'] = strval($beneficiaire->titu_indirect_nue_prop);
		$beneficiaires_advanced[$x]['detail']['Parts détenues indirectement via une personne morale'] = strval($beneficiaire->part_indirect_pmor) . ($beneficiaire->part_indirect_pmor?' %':'');
		$beneficiaires_advanced[$x]['detail']['Parts détenues indirectement via une personne morale en pleine propriété'] = strval($beneficiaire->part_indirect_pmor_plein_prop) . ($beneficiaire->part_indirect_pmor_plein_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Parts détenues indirectement via une personne morale en nue propriété'] = strval($beneficiaire->part_indirect_pmor_nue_prop) . ($beneficiaire->part_indirect_pmor_nue_prop?' %':'');
		//$beneficiaires_advanced[$x]['detail']['titu_indirect_pmor'] = strval($beneficiaire->titu_indirect_pmor);
		//$beneficiaires_advanced[$x]['detail']['titu_indirect_pmor_plein_prop'] = strval($beneficiaire->titu_indirect_pmor_plein_prop);
		//$beneficiaires_advanced[$x]['detail']['titu_indirect_pmor_nue_prop'] = strval($beneficiaire->titu_indirect_pmor_nue_prop);
		$beneficiaires_advanced[$x]['detail']['Droits de vote total'] = strval($beneficiaire->vote_total) . ($beneficiaire->vote_total?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus directement'] = strval($beneficiaire->vote_direct) . ($beneficiaire->vote_direct?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus en pleine propriété'] = strval($beneficiaire->vote_direct_plein_prop) . ($beneficiaire->vote_direct_plein_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus en nue propriété'] = strval($beneficiaire->vote_direct_nue_prop) . ($beneficiaire->vote_direct_nue_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus en usufruit'] = strval($beneficiaire->vote_direct_usufruit) . ($beneficiaire->vote_direct_usufruit?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement'] = strval($beneficiaire->vote_indirect) . ($beneficiaire->vote_indirect?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement via une indivision'] = strval($beneficiaire->vote_indirect_ind) . ($beneficiaire->vote_indirect_ind?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement via une indivision en pleine propriété'] = strval($beneficiaire->vote_indirect_ind_plein_prop) . ($beneficiaire->vote_indirect_ind_plein_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement via une indivision en nue propriété'] = strval($beneficiaire->vote_indirect_ind_nue_prop) . ($beneficiaire->vote_indirect_ind_nue_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement via une indivision en usufruit'] = strval($beneficiaire->vote_indirect_ind_usufruit) . ($beneficiaire->vote_indirect_ind_usufruit?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement via personne(s) morale(s)'] = strval($beneficiaire->vote_indirect_pmor) . ($beneficiaire->vote_indirect_pmor?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement via personne(s) morale(s) en pleine propriété'] = strval($beneficiaire->vote_indirect_pmor_plein_prop) . ($beneficiaire->vote_indirect_pmor_plein_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement via personne(s) morale(s) en nue propriété'] = strval($beneficiaire->vote_indirect_pmor_nue_prop) . ($beneficiaire->vote_indirect_pmor_nue_prop?' %':'');
		$beneficiaires_advanced[$x]['detail']['Droits de vote détenus indirectement via personne(s) morale(s) en usufruit'] = strval($beneficiaire->vote_indirect_pmor_usufruit) . ($beneficiaire->vote_indirect_pmor_usufruit?' %':'');
		$beneficiaires_advanced[$x]['detail']['Autres moyens de contrôle détenus'] = strval($beneficiaire->autre_moyen_controle);
		$beneficiaires_advanced[$x]['detail']['Pouvoir de contrôle sur les décisions des organes d\'administration'] = strval($beneficiaire->pouvoir_decision_ag);
		$beneficiaires_advanced[$x]['detail']['Pouvoir de nommer les membres des organes d\'administration'] = strval($beneficiaire->pouvoir_nommer_membre_ca);
		$beneficiaires_advanced[$x]['detail']['Est le représentant légal'] = strval($beneficiaire->benef_representant_legal);
		$beneficiaires_advanced[$x]['detail']['Nom de la société à qui le placement collectif a délégué la gestion'] = strval($beneficiaire->nom_societe_gestion);
		$beneficiaires_advanced[$x]['detail']['Est le réprésentant légal du placement collectif'] = strval($beneficiaire->rep_legal_plac_sans_gest_delg);
		$beneficiaires_advanced[$x]['detail']['N° SIREN de la société de gestion'] = strval($beneficiaire->siren_societe_gestion);
		$beneficiaires_advanced[$x]['detail']['Greffe d\immatriculation de la société de gestion'] = $greffe[strval($beneficiaire->greffe_societe_gestion)];
		$beneficiaires_advanced[$x]['detail']['Adresse de la société de gestion'] = strval($beneficiaire->adresse_siege_societe_gestion);
		$beneficiaires_advanced[$x]['detail']['Code postal de la société de gestion'] = strval($beneficiaire->code_postal_societe_gestion);
		$beneficiaires_advanced[$x]['detail']['Ville de la société de Gestion'] = strval($beneficiaire->ville_societe_gestion);
		$beneficiaires_advanced[$x]['detail']['Date depuis laquelle la personne n\'est plus bénéficiaire'] = ($beneficiaire->date_suppression)?date('d/m/Y',strtotime(strval($beneficiaire->date_suppression))):null;
		$beneficiaires_advanced[$x]['detail']['Date déclaration au Greffe'] = date('d/m/Y',strtotime(strval($beneficiaire->date_greffe_rbe)));
		$beneficiaires_advanced[$x]['detail']['Date d\'ajout des données'] = date('d/m/Y',strtotime(strval($beneficiaire->date_integration)));
		$x++;
	}
}

if (!$rbeactes)
{
	curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/actes-rbe/find?siren=" . $_GET['siren']);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: JSESSIONID='.$jsessionid));
	curl_setopt($curl, CURLOPT_POST, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	$result = curl_exec($curl);

	$response_rbeactes = json_decode($result);
	foreach($response_rbeactes as $acte)
	{
		$rbeactes[intval($acte->dateDepot)]['type'][] = "Document relatif au bénéficiaire effectif";
		$rbeactes[intval($acte->dateDepot)]['dat_depot'] = intval($acte->dateDepot);
		$rbeactes[intval($acte->dateDepot)]['id_fichier_rbe'] = intval($acte->idFichier);
	}
	
	file_put_contents('rbe/' . $_GET['siren'] . '_actes.json',json_encode($rbeactes));
}
else
	$rbeactes = json_decode($rbeactes,TRUE);

foreach($rbeactes as $key => $value)
	if (array_key_exists($key,$actes))
		$actes[$key]['id_fichier_rbe'] = $value['id_fichier_rbe'];
	else
		$actes[$key] = $value;
?>


