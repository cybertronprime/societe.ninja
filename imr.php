<?php
basename($_SERVER['PHP_SELF']) == basename(__FILE__) && exit;
if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);

if (file_exists('imrs/' . $_GET['siren'] . '.xml'))
{
	if (time() > filemtime('imrs/' . $_GET['siren'] . '.xml') + 86400)
	{
		unlink('imrs/' . $_GET['siren'] . '.xml');
		unlink('imrs/' . $_GET['siren'] . '_actes.json');
	}
	else
	{
		$xml = file_get_contents('imrs/' . $_GET['siren'] . '.xml');
		$actes = file_get_contents('imrs/' . $_GET['siren'] . '_actes.json');
	}
}

if (!$xml)
{
	if (time() > filemtime('imr_token.txt') + 1700)
	{
		curl_setopt($curl, CURLOPT_URL, 'https://opendata-rncs.inpi.fr/services/diffusion/login');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('login: ' . $rncs_login, 'password: ' . $rncs_password));
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HEADER, true);
		$result = curl_exec($curl);

		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$curl_errno= curl_errno($curl);
		if ($http_status==200)
		{
			$jsessionid = substr($result,strpos($result,'JSESSIONID=')+11,32);
			file_put_contents('imr_token.txt',$jsessionid);
		}
		else
			$errors[] = "ERREUR " . $http_status . ' / ' . $curl_errno . " <br/>DATA.INPI.FR est momentanément inaccessible<br/>Les actes, comptes sociaux et données relatives aux dirigeants sont en conséquence indisponibles<br/>Veuillez réessayer ultérieurement";
	}
	else
		$jsessionid = file_get_contents('imr_token.txt');

	curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/imrs-saisis/get?listeSirens=" . $_GET['siren']);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: JSESSIONID='.$jsessionid));
	curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
	curl_setopt($curl, CURLOPT_POST, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	$result = curl_exec($curl);

	$response = json_decode($result);
	if ($response->globalErrors);
		foreach($response->globalErrors as $error)
			$errors[] = 'data.inpi.fr<br/>Fichier Entreprises<br/>' . $error;
	
	//EXTRACTION DU ZIP CONTENANT LE FICHIER XML (le fichier ZIP est traité en mémoire par décomposition de la chaîne binaire)
	//Voir la spécification du format PKZIP : https://users.cs.jmu.edu/buchhofp/forensics/formats/pkzip.html
	//On élimine le "central directory" qui commence par "\x50\x4b\x01\x02".
	//On décompose chaque fichier sachant que la signature de 4 octets commence toujours par "\x50\x4b\x03\x04"
	//Le header de chaque fichier fait 30 octets. Moins la signature, reste 26. +le nom du fichier de longueur variable précisée dans le header.
	//Le footer (data descriptor) fait quant à lui 12 octets.
	//Ce qu'il y a entre le header et le footer est donc le contenu compressé qu'on peut extraire en mémoire avec gzinflate
	//quand il y a un zip dans un zip il y a confusion entre la signature du fichier zip et celle du fichier zip dans le zip
	$filesectors = explode("\x50\x4b\x01\x02", $result);
	$filesectors = $filesectors[0] . "\x50\x4b\x01\x02" . $filesectors[1];
	$filesectors = explode("\x50\x4b\x03\x04", $filesectors);
	$zip_in_zip = $filesectors[1] . "\x50\x4b\x03\x04" . $filesectors[2];
	$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $zip_in_zip);
	$zip = gzinflate(substr($zip_in_zip,26+$filedescription['filename_length'],-12));

	$filesectors = explode("\x50\x4b\x01\x02", $zip);
	$filesectors = explode("\x50\x4b\x03\x04", $filesectors[0]);
	array_shift($filesectors);
	foreach($filesectors as $filesector)
	{
		$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $filesector);
		$filedescription['filename'] = substr($filesector,26,$filedescription['filename_length']);
		if (substr($filedescription['filename'],-3) == 'xml')
			$xml = gzinflate(substr($filesector,26+$filedescription['filename_length'],-12));
	}
	if ($xml AND !$errors)
		file_put_contents('imrs/' . $_GET['siren'] . '.xml',$xml);
	else
		file_put_contents('imrs/' . $_GET['siren'] . '.xml','null');
}

$imr = simplexml_load_string($xml);

if ($imr->dossier)
{
	//DETECTION DU DERNIER GREFFE
	foreach($imr->dossier as $dossier)
		if (!$dossier->identite->dat_rad && $dossier->identite->dat_immat > $derniere_immatriculation)
			$derniere_immatriculation = $dossier->identite->dat_immat;
	
	if (!$derniere_immatriculation)
		foreach($imr->dossier as $dossier)
			if ($dossier->identite->dat_immat > $derniere_immatriculation)
				$derniere_immatriculation = $dossier->identite->dat_immat;
	
	//RENSEIGNEMENTS JURIDIQUES
	foreach($imr->dossier as $dossier)		
		if ($dossier->identite->type_inscrip == 'P' && $dossier->identite->dat_immat == $derniere_immatriculation)
		{		
			if ($dossier->identite->identite_PP)
			{
				$unitelegale['Identité'][] = $dossier->identite->identite_PP->prenom . ' ' . $dossier->identite->identite_PP->nom_patronymique;
				 
				if ($dossier->identite->identite_PP->dat_naiss)
					$unitelegale['Identité'][] = 'Né(e) le ' . date('d/m/Y',strtotime($dossier->identite->identite_PP->dat_naiss)) . ' à ' . $dossier->identite->identite_PP->lieu_naiss . ($dossier->identite->identite_PP->pays_naiss && strtoupper($dossier->identite->identite_PP->pays_naiss) != 'FRANCE'?strtoupper($dossier->identite->identite_PP->pays_naiss):'');
				
				if ($dossier->identite->identite_PP->eirl_indic == 1 OR $dossier->identite->identite_PP->eirl_indic == 'oui')
					$unitelegale['Forme Juridique'] = 'Entreprise Individuelle';
				else
					$unitelegale['Forme Juridique'] = 'Entreprise Individuelle à Responsabilité Limitée';
				
				if ($dossier->identite->identite_PP->adr_siege_ville)
				{
					if ($dossier->identite->identite_PP->adr_siege_1)
						$unitelegale['Siège Social']['adresse1'] = preg_replace('/\s+/', ' ',$dossier->identite->identite_PP->adr_siege_1);
					if ($dossier->identite->identite_PP->adr_siege_2)
						$unitelegale['Siège Social']['adresse2'] = preg_replace('/\s+/', ' ',$dossier->identite->identite_PP->adr_siege_2);
					if ($dossier->identite->identite_PP->adr_siege_3)
						$unitelegale['Siège Social']['adresse3'] = preg_replace('/\s+/', ' ',$dossier->identite->identite_PP->adr_siege_3);
					if ($dossier->identite->identite_PP->adr_siege_ville)
						$unitelegale['Siège Social']['cp ville'] = preg_replace('/\s+/', ' ',$dossier->identite->identite_PP->adr_siege_cp . ' ' . $dossier->identite->identite_PP->adr_siege_ville);
					if ($dossier->identite->identite_PP->adr_siege_pays && strtoupper($dossier->identite->identite_PP->adr_siege_pays) != 'FRANCE')
						$unitelegale['Siège Social']['pays'] = strtoupper($dossier->identite->identite_PP->adr_siege_pays);
				}
			}
			
			if ($dossier->identite->identite_PM)
			{
				$unitelegale['Dénomination Sociale'] = $dossier->identite->identite_PM->denomination;
				
				if ($dossier->identite->identite_divers->adresse_siege)
				{
					preg_match('/[0-9]{5}/', $dossier->identite->identite_divers->adresse_siege, $separator_offset, PREG_OFFSET_CAPTURE);
					if ($separator_offset[0][1] > 0)
					{
						$unitelegale['Siège Social']['adresse'] = substr($dossier->identite->identite_divers->adresse_siege,0,$separator_offset[0][1]);
						$unitelegale['Siège Social']['cp ville'] = substr($dossier->identite->identite_divers->adresse_siege,$separator_offset[0][1]);
					}
					else
						$unitelegale['Siège Social']['adresse'] = $dossier->identite->identite_divers->adresse_siege;
				}
				else
					foreach($dossier->etablissements->etablissement as $etablissement)
						if ($etablissement->type == 'SIE' OR $etablissement->type == 'SEP')
						{
							if ($etablissement->adr_ets_1)
								$unitelegale['Siège Social']['adresse1'] = $etablissement->adr_ets_1;
							if ($etablissement->adr_ets_2)
								$unitelegale['Siège Social']['adresse2'] = $etablissement->adr_ets_2;
							if ($etablissement->adr_ets_3)
								$unitelegale['Siège Social']['adresse3'] = $etablissement->adr_ets_3;
							if ($etablissement->adr_ets_ville)
								$unitelegale['Siège Social']['cp ville'] = $etablissement->adr_ets_cp . ' ' . $etablissement->adr_ets_ville;
							if ($etablissement->adr_ets_pays && strtoupper($etablissement->adr_ets_pays) != 'FRANCE')
								$unitelegale['Siège Social']['pays'] = strtoupper($etablissement->adr_ets_pays);
						}

				if ($dossier->identite->identite_PM->code_form_jur)
					$unitelegale['Forme Juridique'] = $categorie_juridique[intval($dossier->identite->identite_PM->code_form_jur)];
				else
					$unitelegale['Forme Juridique'] = $dossier->identite->identite_PM->form_jur;
				$unitelegale['Capital Social'] = number_format(floatval($dossier->identite->identite_PM->montant_cap),2,',',' ') . ' ' . $dossier->identite->identite_PM->devise_cap;
			}
			
			if ($dossier->identite->dat_immat)
				$unitelegale['Immatriculation'] = date('d/m/Y',strtotime($dossier->identite->dat_immat));
			$unitelegale['Greffe'] = $greffe[strval($dossier->attributes()->code_greffe)];
			
			if ($dossier->identite->identite_divers->ape_naf)
				$unitelegale['Code NAF'] = $dossier->identite->identite_divers->ape_naf;
		}

	//OBSERVATIONS
	foreach($imr->dossier as $dossier)
		if ($dossier->observations)
			foreach($dossier->observations->observation as $observation)
			{
				$observations[intval($observation->date_greffe)]['date_greffe'] = $observation->date_greffe;
				$observations[intval($observation->date_greffe)]['type'] = $type_jugement[strval($observation->code_observation)];
				if ($observation->texte_observation)
				{
					$description = ' ' . substr($observation->texte_observation,21);
					foreach ($type_administrateur as $key=>$value)
						$description = str_replace(' ' . $key,'<br/><b>'. $value.' : </b><br/>',$description);
					$observations[intval($observation->date_greffe)]['description'][] = preg_replace('/\s+/', ' ',substr($description,5));
				}
				else
					$observations[intval($observation->date_greffe)]['description'][] = $observation->texte . '<br/>';
			}

	//REPRESENTANTS
	if ($dossier->identite->identite_PM)
	{
		foreach($imr->dossier as $dossier)		
			if ($dossier->representants && $dossier->identite->dat_immat == $derniere_immatriculation)
				foreach($dossier->representants->representant as $representant)
				{
					if ($representant->libelle_evt == 'TITMC')
						$nom = preg_replace('/\s+/', ' ',strval($representant->raison_nom));
					else if ($representant->type == 'P.Physique')
						$nom = strval($representant->prenoms) . ' ' . strval($representant->nom_usage) . ' ' . strval($representant->nom_patronymique);
					else
						$nom = $representant->denomination . ($representant->siren?' (<a href="/data.php?siren=' . $representant->siren . '">' . $representant->siren . '</a>)':'');
					
					$representants[$nom]['nom'] = $nom;
					
					$representants[$nom]['date_greffe'] = strval($representant->date_greffe);
					$representants[$nom]['type'] = strval($representant->type);
					
					if ($representant->code_qualite)
						$representants[$nom]['qualite'][$qualite_representant[strval($representant->code_qualite)]] = $qualite_representant[strval($representant->code_qualite)];
					else
						foreach($representant->qualites->qualite as $qualite)
							$representants[$nom]['qualite'][ucfirst(strval($qualite))] = ucfirst(strval($qualite));
					
					if ($representant->libelle_evt == 'TITMC')
						$representants[$nom]['nom'] = preg_replace('/\s+/', ' ',strval($representant->raison_nom));
					else if ($representant->type == 'P.Physique')
						$representants[$nom]['nom'] = strval($representant->prenoms) . ' ' . strval($representant->nom_usage) . ' ' . strval($representant->nom_patronymique);
					else
						$representants[$nom]['nom'] = $representant->denomination . ($representant->siren?' (<a href="/data.php?siren=' . $representant->siren . '">' . $representant->siren . '</a>)':'');
								
					if ($representant->dat_naiss)
						$representants[$nom]['naissance'] = 'Né(e) le ' . date('d/m/Y',strtotime($representant->dat_naiss)) . ' à ' . $representant->lieu_naiss . (($representant->pays_naiss)?' ('.$representant->pays_naiss.')':'');
						
					if ($representant->adr_rep_1)
						$representants[$nom]['adresse']['adresse1'] = strval($representant->adr_rep_1);
					if ($representant->adr_rep_2)
						$representants[$nom]['adresse']['adresse2'] = strval($representant->adr_rep_2);
					if ($representant->adr_rep_3)
						$representants[$nom]['adresse']['adresse3'] = strval($representant->adr_rep_3);
					if ($representant->adr_rep_ville)
						$representants[$nom]['adresse']['cp ville'] = $representant->adr_rep_cp . ' ' . strtoupper($representant->adr_rep_ville);
					if ($representant->adr_rep_pays && strtoupper($representant->adr_rep_pays) != 'FRANCE')
						$representants[$nom]['adresse']['pays'] = strtoupper($representant->adr_rep_pays);		
				}
	}

	//BENEFICIAIRES
	if ($dossier->identite->identite_PM)
	{
		$x=0;
		foreach($imr->dossier as $dossier)		
			if ($dossier->beneficiaires && $dossier->identite->dat_immat == $derniere_immatriculation)
				foreach($dossier->beneficiaires->beneficiaire as $beneficaire)
				{
					$beneficiaires[$x]['type_entite'] = strval($beneficaire->type_entite);
					$beneficiaires[$x]['nom_naissance'] = strtoupper(strval($beneficaire->nom_naissance));
					if ($beneficaire->nom_naissance != $beneficaire->nom_usage)
						$beneficiaires[$x]['nom_usage'] = strtoupper(strval($beneficaire->nom_usage));
					$beneficiaires[$x]['prenoms'] = strval($beneficaire->prenoms);
					$beneficiaires[$x]['date_naissance'] = strval($beneficaire->date_naissance);
					$beneficiaires[$x]['nationalite'] = strval($beneficaire->nationalite);
					$beneficiaires[$x]['parts_directes_pleine_propr'] = strval($beneficaire->parts_directes_pleine_propr);
					$beneficiaires[$x]['detention_part_totale'] = strval($beneficaire->detention_part_totale);
					$beneficiaires[$x]['vote_direct_pleine_propriete'] = strval($beneficaire->vote_direct_pleine_propriete);
					$beneficiaires[$x]['detention_vote_total'] = strval($beneficaire->detention_vote_total);
					$beneficiaires[$x]['detention_pouvoir_decision_ag'] = strval($beneficaire->detention_pouvoir_decision_ag);
					$beneficiaires[$x]['deten_pvr_nom_membr_cons_admin'] = strval($beneficaire->deten_pvr_nom_membr_cons_admin);
					$beneficiaires[$x]['detent_autres_moyens_controle'] = strval($beneficaire->detent_autres_moyens_controle);
					$beneficiaires[$x]['benef_reprst_legal'] = strval($beneficaire->benef_reprst_legal);
					$beneficiaires[$x]['rep_legal_placmt_ss_gest_deleg'] = strval($beneficaire->rep_legal_placmt_ss_gest_deleg);
					$beneficiaires[$x]['date_greffe'] = date('d/m/Y',strtotime($beneficaire->date_greffe));
					$beneficiaires[$x]['date_integration'] = date('d/m/Y',strtotime($beneficaire->date_integration));
					$x++;
				}
	}
	
	//ACTES
	if (!$actes)
	{
		foreach($imr->dossier as $dossier)			
			if ($dossier->actes)
				foreach($dossier->actes->acte as $acte)
				{
					$actes[intval($acte->dat_depot)]['dat_depot'] = intval($acte->dat_depot);
					$actes[intval($acte->dat_depot)]['dat_acte'] = str_replace('-','',$acte->dat_acte);
					if (strpos($actes[intval($acte->dat_depot)]['dat_acte'],'/'))
						$actes[intval($acte->dat_depot)]['dat_acte'] = substr($acte->dat_acte,6,4) . substr($acte->dat_acte,3,2) . substr($acte->dat_acte,0,2);
					
					if (strlen($acte->type)==2 AND !in_array($type_acte[strval($acte->type)], $actes[intval($acte->dat_depot)]['type']) AND strval($acte->type!='AA'))
						$actes[intval($acte->dat_depot)]['type'][] = $type_acte[strval($acte->type)];
					else if(strlen($acte->type)!=2 AND !in_array(trim(strval($acte->type)), $actes[intval($acte->dat_depot)]['type']))
						$actes[intval($acte->dat_depot)]['type'][] = trim(strval($acte->type));
					
					if ($acte->code_nature AND !in_array($nature_acte[strval($acte->code_nature)], $actes[intval($acte->dat_depot)]['nature']))
						$actes[intval($acte->dat_depot)]['nature'][] = $nature_acte[strval($acte->code_nature)];
					else if ($acte->decision AND !in_array(trim(strval($acte->decision)), $actes[intval($acte->dat_depot)]['nature']))
						$actes[intval($acte->dat_depot)]['nature'][] = trim(strval($acte->decision));
				}
		
		
		curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/actes/find?siren=" . $_GET['siren']);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: JSESSIONID='.$jsessionid));
		curl_setopt($curl, CURLOPT_POST, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		$result = curl_exec($curl);
		$response_actes = json_decode($result);
		foreach($response_actes as $acte)
		{
			if (!$actes[intval($acte->dateDepot)])
				$actes[intval($acte->dateDepot)]['type'][] = "Inconnu";
			$actes[intval($acte->dateDepot)]['dat_depot'] = intval($acte->dateDepot);
			$actes[intval($acte->dateDepot)]['id_fichier'] = intval($acte->idFichier);
		}
	}
	else
		$actes = json_decode($actes,TRUE);

	//COMPTES ANNUELS
	foreach($imr->dossier as $dossier)	
		if ($dossier->comptes_annuels)	
			foreach($dossier->comptes_annuels->compte_annuel as $compte_annuel)
			{
				$comptes_annuels[intval($compte_annuel->dat_cloture)]['dat_cloture'] = $compte_annuel->dat_cloture;
				$comptes_annuels[intval($compte_annuel->dat_cloture)]['dat_depot'] = $compte_annuel->dat_depot;
				
				if ($compte_annuel->confid_indic == 1 OR strtoupper($compte_annuel->confid_indic)=='OUI')
					$comptes_annuels[intval($compte_annuel->dat_cloture)]['confidentialite'] = 'confidentialité totale';
				else if ($compte_annuel->conf_cr_indic==1)
					$comptes_annuels[intval($compte_annuel->dat_cloture)]['confidentialite'] = 'confidentialité partielle';
				else
					$comptes_annuels[intval($compte_annuel->dat_cloture)]['confidentialite'] = 'publique';
			}
	
	file_put_contents('imrs/' . $_GET['siren'] . '_actes.json',json_encode($actes));
}
?>
