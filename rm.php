<?php
basename($_SERVER['PHP_SELF']) == basename(__FILE__) && exit;
if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);

if (file_exists('rm/' . $_GET['siren'] . '.json'))
{
	if (time() > filemtime('rm/' . $_GET['siren'] . '.json') + 86400)
		unlink('rm/' . $_GET['siren'] . '.json');
	else
		$rm_data = file_get_contents('rm/' . $_GET['siren'] . '.json');
}

//ETABLISSEMENTS
if (!$rm)
{	
	curl_setopt($curl, CURLOPT_URL, "https://api-rnm.artisanat.fr/v2/entreprises/" . $_GET['siren']);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$json = curl_exec($curl);
	$rm_data = json_decode($json);
	file_put_contents('rm/' . $_GET['siren'] . '.json',$json);
}
else
	$rm_data = json_decode($rm);

if ($rm_data->id)
{
	$rm['Chambre'] = $rm_data->gest_dept . ' (' . $rm_data->gest_reg . ')';
	$rm['N° de Gestion'] = $rm_data->ent_id_num_gestion;
	$rm['Immatriculation'] = date('d/m/Y',strtotime($rm_data->ent_act_date_immat_rm));
	$rm['Qualification Dirigeant'] = $rm_data->dir_qa_qualification;
}
?>
