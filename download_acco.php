<?php
if ($_GET['id'] && !preg_match('/^[A-Z0-9]+$/',$_GET['id'])) die ('Invalid id : ' . $_GET['id']);

include('config.php');
$curl = curl_init();
//LOGIN
curl_setopt($curl, CURLOPT_URL, "https://oauth.aife.economie.gouv.fr/api/oauth/token");
curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=" . $piste_client_id . "&client_secret=" . $piste_secret . "&scope=openid");
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept-Encoding: gzip,deflate",
"Content-Type: application/x-www-form-urlencoded",
"Content-Length: 140",
"Host: oauth.aife.economie.gouv.fr",
"Connection: Keep-Alive",
"User-Agent: Apache-HttpClient/4.1.1 (java 1.5)"
));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, 0);
$result = curl_exec($curl);
$access_token = json_decode($result);
$access_token = $access_token->access_token;



$params = '{"id":"' . $_GET['id'] . '"}';
curl_setopt($curl, CURLOPT_URL, "https://api.aife.economie.gouv.fr/dila/legifrance-beta/lf-engine-app/consult/acco");
curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
"Authorization: Bearer ". $access_token,
"Accept-Encoding: gzip,deflate",
"Content-Type: application/json",
"Content-Length: " . strlen($params),
"Connection: Keep-Alive",
"User-Agent: Apache-HttpClient/4.1.1 (java 1.5)",
));
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_HEADER, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
$result = json_decode($result);

$file = $result->acco->data;
$file = base64_decode($file);

curl_close($curl);

if ($_GET['method']=='inline')
{
	header('Content-type: application/msword');
	header('Content-Disposition: inline; filename="' . substr(str_replace('/','-',$result->acco->attachementUrl),1));
}
else
{
	header('Content-Description: File Transfer');
	header('Content-Type: application/msword');
	header('Content-Length: ' . strlen($file));
	header('Content-Disposition: inline; filename="' . substr(str_replace('/','-',$result->acco->attachementUrl),1));
}

echo $file;

?>
