<?php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, "https://data.inpi.fr/search");
curl_setopt($curl, CURLOPT_POSTFIELDS, '{"query":{"type":"drawing_models","selectedIds":[],"sort":"relevance","order":"asc","nbResultsPerPage":"1000","page":"1","filter":{},"q":"' . $_GET['siren'] . '"},"aggregations":["classifications.classDescription.classNumber","registrationOfficeCode"]}');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);

$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
if ($http_status>=500)
	$errors[] = "ERREUR " . $http_status . " <br/>L'API \"Dessins & Modèles\" est momentanément inaccessible<br/>Veuillez réessayer ultérieurement";

if ($_GET['format'] == 'json')
	die(print_r($result));
$result = json_decode($result);

foreach ($result->result->hits->hits as $design)
{
	$design = $design->_source;
		
	$designs[$design->applicationDate] = array
	(
		"numero" => $design->DesignApplicationNumber,
		"office" => $design->registrationOfficeCode,
		"reference" => $design->designReference,
		"nom" => $design->designTitle[0]->text,
		"depot" => date('d/m/Y',$design->applicationDate/1000),
		"expiration" => date('d/m/Y',$design->expiryDate/1000),
	);
}
?>
