<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>RCS NINJA</title>
		<meta name="viewport" content="width=device-width, initial-scale=0.75"/>
		<meta name="description" content="Accès gratuit aux informations sur les entreprises et sociétés françaises (statuts, PV, procès verbaux, comptes annuels, bilans...)"/>
		<link rel="stylesheet" href="index.css"/>
		<link href="https://fonts.googleapis.com/css2?family=Nobile&family=Roboto&display=swap" rel="stylesheet"/>
		<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon.png"/>
	</head>
	<body style="overflow:auto">
		<br/>
		<b><u>Ce site est édité par :</u></b><br/><br/>
		Lionel VEST<br/>Avocat au Barreau de Strasbourg<br/>11B Rue de Madrid<br/>67300 SCHILTIGHEIM<br/>FRANCE<br/><a href="mailto:lionel.vest@adaris.org">lionel.vest@adaris.org</a><br/>
		<br/><br/>
		<b><u>Ce site est hébergé par :</u></b><br/><br/>
		OVH<br/>SAS au capital de 10 069 020 €<br/>RCS Lille Métropole 424 761 419 00045<br/>2 rue Kellermann<br/>59100 ROUBAIX<br/>FRANCE<br/>
		<br/><br/>
		<b><u>Origine des données :</u></b><br/><br/>
		Notre site ne stocke ou traite aucune donnée relative aux entreprises ou à leurs dirigeants.<br/>
		Les données affichées sont fournies en OPEN DATA par des API externes :<br/><br/>
		INSEE (Répertoire SIRENE) : <a href="https://api.insee.fr">api.insee.fr</a><br/>
		INPI (Répertoire RNCS) : <a href="https://data.inpi.fr">data.inpi.fr</a><br/>
		LEGIFRANCE : <a href="https://aife.economie.gouv.fr/">api.aife.economie.gouv.fr/dila/legifrance-beta</a><br/>
		BODACC : <a href="https://www.bodacc.fr">www.bodacc.fr</a><br/><br/>
		Toutes réclamations relatives aux données (suppression, modification)<br/>doivent être adressées directement aux éditeurs de ces bases publiques.<br/>
		<br/><br/>
		<b><u>Conditions d'utilisation :</u></b><br/><br/>
		L'utilisation du présent site implique l'acceptation pleine et entière des conditions générales d'utilisation décrites ci-après.<br/>
		Les informations diffusées sont présentés à titre indicatif, ne revêtent pas un caractère exhaustif, et ne peuvent engager la responsabilité de l'éditeur de ce site.<br/>
		Ce dernier ne peut être tenu responsable des dommages directs ou indirects consécutifs à l'utilisation du site.<br/>
		L'éditeur décline également toute responsabilité et n’est pas engagé par le référencement de ressources externes à ce site.<br/>
		L'INPI imposant des quotas de consultations quotidiennes, l'éditeur se réserve le droit discrétionnaire de limiter les utilisateurs effectuant trop de recherches.<br/>
		Pour la même raison, merci de ne faire de lien que vers la page d'accueil : <a href="https://www.societe.ninja">www.societe.ninja</a>.<br/>
		<br/><br/>
		<b>Audit de sécurité gracieusement offert par <a href="https://www.ziwit.com/fr/">ZIWIT</a></b><br/><br/>
	</body>
</html>