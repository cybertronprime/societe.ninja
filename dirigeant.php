<?php
$_GET['dirigeant'] = preg_replace('/[^0-9a-zA-Zàâäéèêëïîôöùûüÿç\-\s]/', '', urldecode($_GET['dirigeant']));


include('config.php');

if ($_GET['format'] != 'json')
{
	echo '<!DOCTYPE html>';
	echo '<html lang="fr">';
	echo '<head>';
		echo '<title>SOCIETE NINJA</title>';
		if (basename($_SERVER['PHP_SELF']) != 'index.php' OR $_GET['denomination'])
			echo '<meta name="robots" content="noindex">';
		echo '<meta name="robots" CONTENT="nofollow">';
		echo '<meta name="viewport" content="width=device-width, initial-scale=0.9"/>';
		echo '<meta name="description" content="Accès gratuit aux informations sur les entreprises et sociétés françaises (statuts, PV, procès verbaux, comptes annuels, bilans...)"/>';
		echo '<link rel="stylesheet" href="index.css"/>';
		echo '<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon.png"/>';
	echo '</head>';
	echo '<body style="overflow:auto">';

	echo '<div class="title_table">';
		echo '<span class="title">SOCIETE.NINJA</span><br/>';
		echo '<span class="subtitle">Informations publiques et gratuites sur les entreprises</span>';
	echo '</div>';

	echo '<form id="menu_recherche" action="dirigeant.php" method="get" autocomplete="off" role="presentation">';
		echo '<div style="text-align:center">';
		if (!$_GET['dirigeant'])
		echo '<img alt="logo" style="width:150px;margin:20px;animation:slide-in-blurred-bottom 0.6s cubic-bezier(0.230, 1.000, 0.320, 1.000) both" src="/images/ninja.png"/>';
		echo '<br/>';
		
			echo '<label for="denomination">DIRIGEANT (Prénom Nom)</label><br/>';
			echo '<input type="text" style="text-transform:uppercase" name="dirigeant" id="dirigeant" value="' . urldecode($_GET['dirigeant']) . '" required autofocus><br/><br/>';
			echo '<input type="text" name="page" value="1" hidden/>';
			echo '<input type="submit" value="Rechercher">';
		echo '</div>';
	echo '</form>';
}


if ($_GET['dirigeant'])
{
	include('constants.php');
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, "https://data.inpi.fr/search");
	curl_setopt($curl, CURLOPT_POSTFIELDS, '{"query":{"type":"companies","selectedIds":[],"sort":"relevance","order":"asc","nbResultsPerPage":"20","page":"' . ($_GET['page']?$_GET['page']:1) . '","filter":{},"q":"' . urldecode($_GET['dirigeant']) . '"},"aggregations":["idt_cp_short","idt_pm_form_jur"]}');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	if ($_GET['format'] == 'json')
		die(print_r($result));
	$result = json_decode($result);

	curl_close($curl);
	
	foreach($result->result->hits->hits as $etablissement)
	{
		foreach ($etablissement->_source->etablissements as $unite)
			if ($unite->type == "SIE" OR $unite->type == "SEP" OR $unite->type == "PRI")
				$siege = $unite;
		
		$dirigeant = $etablissement->_source->representants[0];
		foreach ($etablissement->_source->representants as $representant)
			if ( stristr($_GET['dirigeant'], explode(' ',$representant->prenoms)[0]) AND stristr(end(explode(' ',$_GET['dirigeant'])), $representant->nom_prenoms[0]))
				$dirigeant = $representant;
		
		$resultats[] = array
		(
			'siren' => $etablissement->_source->siren,
			'denomination' => array
			(
				$etablissement->_source->idt_pm_denomination,
			),
			'naf' => ($etablissement->_source->idt_divers_ape_naf ? substr($etablissement->_source->idt_divers_ape_naf,0,2).'.'.substr($etablissement->_source->idt_divers_ape_naf,2) : ''),
			'adresse' => array
			(
				// $etablissement->_source->etablissements[0]->complementAdresseEtablissement,
				// $etablissement->_source->etablissements[0]->numeroVoieEtablissement.' '.$etablissement->adresseEtablissement->indiceRepetitionEtablissement.' '.$etablissement->adresseEtablissement->typeVoieEtablissement.' '.$etablissement->adresseEtablissement->libelleVoieEtablissement,
				$siege->adr_ets_1 . ' ' . $siege->adr_ets_2 . ' ' . $siege->adr_ets_3,
				$siege->adr_ets_cp . ' ' . $siege->adr_ets_ville,
				// $etablissement->_source->etablissements[0]->distributionSpecialeEtablissement,
				// $etablissement->_source->etablissements[0]->codeCedexEtablissement.' '.$etablissement->adresseEtablissement->libelleCedexEtablissement,
				// $etablissement->_source->etablissements[0]->libellePaysEtrangerEtablissement,
			),
			'representant' => array
			(
				$dirigeant->qualites,
				$dirigeant->nom_prenoms,
				(($dirigeant->date_naiss>0 OR $dirigeant->lieu_naiss) ? "Né" : '') . ($dirigeant->date_naiss>0 ? " le " . date('d/m/Y',($dirigeant->date_naiss/1000)) : "") . ($dirigeant->lieu_naiss ? ' à ' . $dirigeant->lieu_naiss : ''),
			),
		);
	}
	
	if ($result->result->hits->total->value == 0)
		die('<br/><br/>Aucun résultat<br/>');
	
	echo '<table class="responsive_table resultats">';
		echo '<thead>';
			echo '<tr><td colspan="5">' . $result->result->hits->total->value . ' RESULTATS</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($resultats as $siren => $resultat)
		{
			echo '<tr style="cursor:pointer" onclick="window.location.href = \'data.php?siren=' . $resultat['siren'] . '\'" oncontextmenu="window.open(\'data.php?siren=' . $resultat['siren'] . '\');return false">';
				echo '<td style="max-width:500px">' . implode('<br/>',array_filter(array_map('trim', $resultat['denomination']))) . '</td>';
				echo '<td>' . $resultat['siren'] . '</td>';
				echo '<td style="max-width:500px">' . implode('<br/>',array_filter(array_map('trim', $resultat['adresse']))) . '</td>';
				echo '<td style="max-width:500px">' . $resultat['naf'] . '<br/>' . $naf[$resultat['naf']] . '</td>';
				echo '<td style="max-width:500px">' . implode('<br/>',array_filter(array_map('trim', $resultat['representant']))) . '</td>';
			echo '</tr>';
		}
		echo '</tbody>';
		echo '<tfoot>';
			echo '<tr>';
				echo '<td colspan="5" style="text-align:center">';
				echo '<div style="text-align:left">' . ($_GET['page'] > 1 ? '<a href="?' . str_replace('&page='.$_GET['page'], '&page='.($_GET['page']-1),$_SERVER['QUERY_STRING']) . '">&#9668; Précédent</a>':'') . '</div>&nbsp;&nbsp;';
				echo '<div style="text-align:center">' . (($_GET['page']-1) * 20 + 1) . ' à ' . min(($_GET['page']-1) * 20 + 21, $result->result->hits->total->value) . ' sur ' . $result->result->hits->total->value . '</div>';
				echo '<div style="text-align:right">' . ($_GET['page'] < $result->result->hits->total->value/20 ? '<a href="?' . str_replace('&page='.$_GET['page'], '&page='.($_GET['page']+1),$_SERVER['QUERY_STRING']) . '">Suivant &#9658;</a>':'') . '</div>';
				echo '</td>';
			echo '</tr>';
		echo '</tfoot>';
	echo '</table>';

	echo '<div style="position:fixed;top:0;left:0"><a href="index.php"><img alt="home" class="menu_icon" style="filter:invert(1)" src="/images/home.svg"/></a></div>';

	echo '<br/><br/>';
}
?>
