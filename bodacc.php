<?php
basename($_SERVER['PHP_SELF']) == basename(__FILE__) && exit;
if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);

if (file_exists('bodacc/' . $_GET['siren'] . '.json'))
{
	if (time() > filemtime('sirene/' . $_GET['siren'] . '_etablissements.json') + 86400)
		unlink('bodacc/' . $_GET['siren'] . '.json');
	else
		$annonces = json_decode(file_get_contents('bodacc/' . $_GET['siren'] . '.json'),TRUE);
}

if (!$annonces)
{
	function bodacc($siret, $page)
	{
		global $annonces;
		
		$postdata = http_build_query(array('registre' => $siret, 'page' => $page));
		$opts = array('http' =>array('method'  => 'POST','header'  => 'Content-type: application/x-www-form-urlencoded','content' => $postdata));
		$context  = stream_context_create($opts);
		$result = file_get_contents('https://www.bodacc.fr/annonce/liste', false, $context);

		if ($page==1)
		{
			global $nb_annonces, $nb_pages;
			$nb_annonces = substr($result,strpos($result,"Nombre d'annonces trouv&eacute;es : ")+36,16);
			$nb_annonces = substr($nb_annonces,0,strpos($nb_annonces,'</h3>'));
			$nb_pages = intval(($nb_annonces-1) / 10) + 1;
		}
		
		$result = substr($result,strpos($result,'<tr class="pair">'));
		$result = substr($result,0,strpos($result,'</table>'));
		
		if ($result != "")
		{
			$DOM = new DOMDocument;
			$DOM->loadHTML($result);
			$trs = $DOM->getElementsByTagName('tr');
			foreach ($trs as $tr)
			{
				$dd = $tr->getElementsByTagName('dd');
				$a = $tr->getElementsByTagName('a');
						
				$annonces[] = array(
				'date'  => $tr->childNodes->item(0)->nodeValue,
				'type'  => $dd->item(2)->nodeValue,
				'greffe' => utf8_decode(end(explode('DE ',$dd->item(4)->nodeValue))),
				'lien'  => 'https://bodacc.fr' . $a->item(0)->getAttribute('href'));
			}
		}
		
		return $annonces;
	}
	
	bodacc($_GET['siren'],1);
	if ($nb_pages >= 2)
		for ($i=2; $i<=$nb_pages; $i++)
			bodacc(substr($_GET['siren'],0,9),$i);
	
	file_put_contents('bodacc/' . $_GET['siren'] . '.json', json_encode($annonces));
}
?>