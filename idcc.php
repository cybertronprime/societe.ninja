<?php
if (file_exists('idcc/' . $_GET['siren'] . '.json'))
{
	if (time() > filemtime('idcc/' . $_GET['siren'] . '.json') + 86400)
		unlink('idcc/' . $_GET['siren'] . '.json');
	else
		$idcc = file_get_contents('idcc/' . $_GET['siren'] . '.json');
}

if (!$idcc)
{
	foreach($etablissements as $etablissement)
		if ($etablissement['siege'] == 1)
			$idcc = file_get_contents('https://siret2idcc.fabrique.social.gouv.fr/api/v2/' . $etablissement['siret']);
	file_put_contents('idcc/' . $_GET['siren'] . '.json',$idcc);
	$idcc = json_decode($idcc);
}
else
	$idcc = json_decode(file_get_contents('idcc/' . $_GET['siren'] . '.json'));

file_put_contents('imrs/' . $_GET['siren'] . '_actes.json',json_encode($actes));

foreach ($idcc[0]->conventions as $convention)
	$unitelegale['Convention Collective'][] = '<a href="javascript:window.open(\'' . $convention->url . '\')">' . $convention->nature . ' ' . $convention->num . ' : ' . $convention->shortTitle . '</a>';
?>