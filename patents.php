<?php
if (strpos($_SERVER['REQUEST_URI'], basename(__FILE__)) !== false)
	$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, "https://data.inpi.fr/search");
curl_setopt($curl, CURLOPT_POSTFIELDS, '{"query":{"type":"patents","selectedIds":[],"sort":"relevance","order":"asc","nbResultsPerPage":"1000","page":"1","filter":{},"q":"' . $_GET['siren'] . '"},"aggregations":["origin"]}');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);

$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
if ($http_status>=500)
	$errors[] = "ERREUR " . $http_status . " <br/>L'API \"Brevets\" est momentanément inaccessible<br/>Veuillez réessayer ultérieurement";

if ($_GET['format'] == 'json')
	die(print_r($result));
$result = json_decode($result);

foreach ($result->result->hits->hits as $brevet)
{
	$brevet = $brevet->_source;
		
	$brevets[$brevet->applicationReference->date] = array
	(
		"numero" => $brevet->publicationReference->docNumber,
		"office" => $brevet->publicationReference->country,
		"reference" => $brevet->publicationReference->kind,
		"nom" => $brevet->inventionTitle,
		"depot" => date('d/m/Y',$brevet->applicationReference->date/1000),
		"expiration" => date('d/m/Y',$brevet->frNextFeePayement/1000),
	);
}
?>
