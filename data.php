<?php
if (strpos($_SERVER['HTTP_ORIGIN'],'104.143.204.')>-1 OR strpos($_SERVER['HTTP_ORIGIN'],'209.220.97.')>-1 OR strpos($_SERVER['HTTP_ORIGIN'],'91.165.91.97')>-1)
	exit;

if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren');

if ($_GET['refresh'])
{
	unlink('sirene/' . $_GET['siren'] . '_etablissements.json');
	unlink('sirene/' . $_GET['siren'] . '_unitelegale.json');
	unlink('imrs/' . $_GET['siren'] . '.xml');
	unlink('rm/' . $_GET['siren'] . '.json');
	unlink('bodacc/' . $_GET['siren'] . '.json');
	unlink('legifrance_acco/' . $_GET['siren'] . '.json');
	unlink('legifrance_acco/' . $_GET['siren'] . '_brut.json');
}

include('config.php');
include('constants.php');
$curl = curl_init();
include('imr.php');
if ($rbe_login != ')
	include('rbe.php');
include('sirene.php');
include('rm.php');
include('legifrance_acco.php');
include('idcc.php');
include('trademarks.php');
include('patents.php');
include('designs.php');
curl_close($curl);
include('bodacc.php');



if ($xml)
	if($_GET['format']=='xml')
	{
		header('Content-type: text/xml');
		die ($xml);
	}
	else if($_GET['format']=='xmldownload')
	{
		header('Content-type: text/xml');
		header('Content-Disposition: attachment; filename="' . $_GET['siren'] . '.xml"');
		die ($xml);
	}

if ($sirene)
	if($_GET['format']=='json')
	{
		header('Content-type: application/json');
		die(json_encode($sirene));
	}
	else if($_GET['format']=='jsondownload')
	{
		header('Content-type: application/json');
		header('Content-Disposition: attachment; filename="' . $_GET['siren'] . '.json"');
		die(json_encode($sirene));
	}

if ($_GET['dev'])
	echo json_encode($sirene);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<title>SOCIETE NINJA (<?=$_GET['siren'];?>)</title>
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=0.9">
	<link rel="stylesheet" href="index.css">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon.png"/>
</head>

<body>


<?php
if ($unitelegale)
{
	echo '<table class="header">';
		echo '<tr>';
			echo '<td id="header_links">';
				if (sizeof($unitelegale) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_unite_legale\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Unite Legale</a>';
				if (sizeof($rm) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_artisan\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Chambre des Métiers</a>';
				if (sizeof($historique_unitelegale) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_historique\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Historique</a>';
				if (sizeof($observations) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_observations\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Observations</a>';
				if (is_array($annonces))
					echo '<a href="javascript:document.getElementById(\'menu_bodacc\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">BODACC</a>';
				if (sizeof($representants) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_representants\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Représentants</a>';
				if (sizeof($beneficiaires) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_beneficiaires\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Bénéficiaires</a>';
				if (is_array($actes) AND sizeof($actes) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_actes\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Actes</a>';
				if (sizeof($comptes_annuels) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_comptes\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Comptes</a>';
				if (sizeof($etablissements) > 0)
					echo '<a href="javascript:document.getElementById(\'menu_etablissements\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Etablissements</a>';
				if (is_array($etablissements_avec_accords))
					echo '<a href="javascript:document.getElementById(\'menu_accords\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Accords</a>';
				if (is_array($marques))
					echo '<a href="javascript:document.getElementById(\'menu_marques\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Marques</a>';
				if (is_array($brevets))
					echo '<a href="javascript:document.getElementById(\'menu_brevets\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Brevets</a>';
				if (is_array($designs))
					echo '<a href="javascript:document.getElementById(\'menu_designs\').scrollIntoView({behavior: \'instant\', block:\'start\', inline:\'nearest\'});menu()">Modèles</a>';
			echo '</td>';
		echo '</tr>';
	echo '</table>';
}

echo '<div class="container">';

echo '<div class="company_name">' . strtoupper($unitelegale['Dénomination Sociale'] . ' ' . $unitelegale['Prénom'] . ' ' . $unitelegale['Nom']) . '</div>';

if ($errors)
	echo '<div style="color:#FF0000;padding:10px">' . implode('<br/>',$errors) . '</div>';

if (!$_GET['siren'])
	exit;
else if (!is_numeric($_GET['siren']))
	die('<center>SIREN invalide<br/>Le numéro SIREN doit être composé de 9 chiffres</center>');

if (!$unitelegale)
	die('AUCUN RESULTAT TROUVE');

//UNITE LEGALE
if (sizeof($unitelegale) > 0)
{
	echo '<table id="menu_unite_legale" class="scrollable_table">';
		echo '<thead><tr><td colspan="2" style="text-align:left">UNITE LEGALE</td></tr></thead>';
		echo '<tbody>';
		foreach($unitelegale as $key => $value)
			echo '<tr><td style="white-space:nowrap">' . $key . '</td><td style="width:100%;overflow-wrap:anywhere">' . (is_array($value)?implode('<br/>',array_filter(array_map('trim', $value))):$value) . '</td></tr>';
		echo '</tbody>';
	echo '</table>';
}

//CHAMBRE DES METIERS
if (sizeof($rm) > 0)
{
	echo '<table id="menu_artisan" class="scrollable_table">';
		echo '<thead><tr><td colspan="2" style="text-align:left">CHAMBRE DES METIERS</td></tr></thead>';
		echo '<tbody>';
		foreach($rm as $key => $value)
			echo '<tr><td style="white-space:nowrap">' . $key . '</td><td style="width:100%;overflow-wrap:anywhere">' . (is_array($value)?implode('<br/>',array_filter(array_map('trim', $value))):$value) . '</td></tr>';
		echo '</tbody>';
	echo '</table>';
}

//HISTORIQUE
if (sizeof($historique_unitelegale) > 0)
{
	echo '<table id="menu_historique" class="scrollable_table">';
		echo '<thead><tr><td colspan="2">HISTORIQUE</td></tr></thead>';
		echo '<tbody>';
		foreach($historique_unitelegale as $evenement)
			echo '<tr><td style="white-space:nowrap">' . date('d/m/Y',strtotime($evenement['date'])) . '</td><td style="width:100%;overflow-wrap:anywhere">' . $evenement['description'] . '</td></tr>';
		echo '</tbody>';
	echo '</table>';
}

//OBSERVATIONS
if (sizeof($observations) > 0)
{
	krsort($observations);
	echo '<table id="menu_observations" class="scrollable_table">';
		echo '<thead><tr><td colspan="2">OBSERVATIONS</td></tr></thead>';
		echo '<tbody>';
		foreach($observations as $observation)
		{
			echo '<tr>';
				echo '<td style="white-space:nowrap">' . date('d/m/Y',strtotime($observation['date_greffe'])) . '</td>';
				echo '<td style="width:100%;overflow-wrap:anywhere">' . ($observation['type']?'<span style="color:#FF0000;font-weight:bold">' . strtoupper($observation['type']) . '</span><br/>':'') . implode('<br/>',$observation['description']) . '</td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}

//ANNONCES BODACC
if (is_array($annonces))
{
	echo '<table id="menu_bodacc" class="scrollable_table bodacc responsive_table">';
		echo '<thead>';
			echo '<tr><td colspan="4">ANNONCES BODACC</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($annonces as $annonce)
		{
			echo '<tr>';
				echo '<td style="white-space:nowrap;text-align:center">' . $annonce['date'] . '</td>';
				echo '<td style=text-align:center">' . ($annonce['greffe']?$annonce['greffe']:'') . '</td>';
				echo '<td style="width:100%">' . ($annonce['type']?$annonce['type']:'') . '</td>';
				echo '<td style="white-space:nowrap;text-align:center" class="print_hide"><a href="' . $annonce['lien'] . '" target="_blank"><img alt="html" class="icon" src="/images/html.svg"/><span class="smartphone_hide"><br/>Afficher</a></span></td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}
	
//REPRESENTANTS
if (sizeof($representants) > 0)
{
	krsort($representants);
	echo '<table id="menu_representants" class="scrollable_table representants responsive_table">';
		echo '<thead><tr><td colspan="3">REPRESENTANTS</td></tr></thead>';
		echo '<tbody>';
		foreach($representants as $representant)
		{		
			echo '<tr>';						
				echo '<td><img class="smartphone_hide" alt="type" style="width:48px;filter:drop-shadow(1px 1px 1px rgba(80,80,80,.7))" src="/images/' . (strpos($representant['type'],"Morale")?'corporation':'person') . '.svg"/></td>';
				echo '<td>' . implode('<br/>',$representant['qualite']) . '</td>';
				echo '<td width="100%">' . ($representant['nom']?$representant['nom'] . '<br/>':'') . ($representant['naissance']?$representant['naissance'] . '<br/>':'') . implode('<br/>',$representant['adresse']) . '</td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}

//BENEFICIAIRES EFFECTIFS
if (sizeof($beneficiaires) > 0 OR sizeof($beneficiaires_advanced) > 0)
{
	echo '<table id="menu_beneficiaires" class="scrollable_table representants responsive_table">';
		echo '<thead><tr><td colspan="3">BENEFICIAIRES EFFECTIFS' . ($rbe_login != ''?'<span style="font-style:italic;color:#FF0000"> (Confidentiel)</span>':'') . '</td></tr></thead>';
		echo '<tbody>';
		if (sizeof($beneficiaires_advanced) > 0)
			foreach($beneficiaires_advanced as $beneficiaire_advanced)
			{		
				echo '<tr>';
					echo '<td style="white-space:nowrap">';
						echo '<b>' . $beneficiaire_advanced["Prénoms"] . ($beneficiaire_advanced["Nom de Naissance"]!=$beneficiaire_advanced["Nom d'Usage"]?' ' .$beneficiaire_advanced["Nom d'Usage"]:'') . ' ' . $beneficiaire_advanced["Nom de Naissance"] . ($beneficiaire_advanced["Pseudonyme"]?' alias ' . $beneficiaire_advanced["Pseudonyme"]:'') . '</b><br/>';
						echo "Nationalité : " . $beneficiaire_advanced["Nationalité"] . '<br/>';
						echo 'Né(e) le ' . $beneficiaire_advanced["Date de naissance"] . ' à ' . $beneficiaire_advanced['Ville de Naissance'] . ' (' . (strtoupper($beneficiaire_advanced['Pays de Naissance'])=='FRANCE'?$beneficiaire_advanced['Département de naissance']:$beneficiaire_advanced['Pays de Naissance']) . ')<br/>'; 
						echo $beneficiaire_advanced["Adresse"] . '<br/>';
						echo $beneficiaire_advanced["Code Postal"] . ' ' . $beneficiaire_advanced["Ville"] . ' (' . $beneficiaire_advanced["Pays"] . ')<br/>';
					echo '</td>';
					echo '<td width="100%">';
					foreach($beneficiaire_advanced['detail'] as $key => $value)
						if ($value)
							echo $key . ' : ' . $value . '<br/>';
					echo '</td>';
				echo '</tr>';
			}
		else
			foreach($beneficiaires as $beneficiaire)
			{		
				
				echo '<tr>';
					//echo '<td><img class="smartphone_hide" alt="type" style="width:48px;filter:drop-shadow(1px 1px 1px rgba(80,80,80,.7))" src="/images/' . (strpos($representant['type'],"Morale")?'corporation':'person') . '.svg"/></td>';
					//echo '<td>' . implode('<br/>',$representant['qualite']) . '</td>';
					echo '<td style="white-space:nowrap">';
						echo '<b>' . $beneficiaire['prenoms'] . ' ' . $beneficiaire['nom_usage'] . ' ' . $beneficiaire['nom_naissance'] . '</b><br/>';
						echo 'Nationalité : ' . $beneficiaire['nationalite'] . '<br/>';
						echo 'Né en ' . $beneficiaire['date_naissance'];
					echo '</td>';
					echo '<td width="100%">';
						echo ($beneficiaire['detention_part_totale']?'Détention : ' . $beneficiaire['detention_part_totale'] . ' %<br/>':'');
						echo ($beneficiaire['detention_vote_total']?'Droit de vote : ' . $beneficiaire['detention_vote_total'] . ' %':'');
					echo '</td>';
				echo '</tr>';
			}
			
		echo '</tbody>';
	echo '</table>';
}

//ACTES
if (is_array($actes) AND sizeof($actes) > 0)
{
	krsort($actes);
	echo '<table id="menu_actes" class="scrollable_table actes responsive_table">';
		echo '<thead>';
			echo '<tr><td colspan="4">ACTES</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($actes as $acte)
		{
			echo '<tr>';
				echo '<td style="text-align:center">' . ($acte['dat_acte']?'<span class="smartphone_hide">Date<br/></span>'.date('d/m/Y',strtotime($acte['dat_acte'])):'') . '</td>';
				echo '<td style="text-align:center"><span class="smartphone_hide">Dépôt<br/></span>' . date('d/m/Y',strtotime($acte['dat_depot'])) . '</td>';
				echo '<td style="width:100%"><b>' . implode('<br/>',$acte['type']) . '</b><br/>' . implode('<br/>',$acte['nature']) . '</td>';
				echo '<td style="text-align:center" class="print_hide">';
				if ($acte['dat_depot'] >= 19930101)
				{
					$count=0;
					foreach($acte['type'] as $type)
						if(stripos($type,'bénéficiaire effectif') OR stripos($type,'bénéficiaires effectifs'))
							$count++;
					if ($count == sizeof($acte['type']) AND $rbe_login == '')
						echo '<img alt="lock" class="icon" src="/images/lock.svg" onclick="alert(\'Ce document n\\\'est pas disponible au public\')"/><span class="smartphone_hide"><br/>Confidentiel</span>';
					else if ($acte['id_fichier'])
						echo '<a href="' . '/download.php?type=acte&filetype=pdf&id_fichier=' . $acte['id_fichier'] . '"><img alt="pdf" class="icon" src="/images/pdf.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a>';	
					else if($rbe_login == '')
						echo '<img alt="unavailable" class="icon" src="/images/unavailable.svg" onclick="alert(\'Le document est indisponible pour une raison indéterminée\')"/><span class="smartphone_hide"><br/>Indisponible</span>';

						if ($acte['id_fichier_rbe'] AND $rbe_login != '')
						echo '<a href="' . '/download_rbe.php?id_fichier=' . $acte['id_fichier_rbe'] . '"><img alt="pdf" class="icon" src="/images/pdf.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a><br/><span style="font-style:italic;color:#FF0000">Confidentiel</span>';	
				}
				else
					echo '<img alt="unavailable" class="icon" src="/images/unavailable.svg" onclick="alert(\'Les documents déposés avant le 01/01/1993 ne sont pas disponibles\')"/>';
				echo '</td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}

//COMPTES ANNUELS
if (sizeof($comptes_annuels) > 0)
{
	krsort($comptes_annuels);
	echo '<table id="menu_comptes" class="scrollable_table">';
		echo '<thead>';
			echo '<tr><td colspan="3">COMPTES ANNUELS</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($comptes_annuels as $compte_annuel)
		{
			echo '<tr>';
				echo '<td style="text-align:center">Clôture<br/>' . date('d/m/Y',strtotime($compte_annuel['dat_cloture'])) . '</td>';
				echo '<td style="text-align:center">Dépôt<br/>' . date('d/m/Y',strtotime($compte_annuel['dat_depot'])) . '</td>';
				echo '<td style="text-align:center;width:100%" class="print_hide">';
				if ($compte_annuel['dat_depot'] >= 20170101 AND $compte_annuel['confidentialite'] != 'confidentialité totale')
				{
					echo '<div style="display:inline-block;margin:5px"><a href="' . '/download.php?type=bilan&filetype=pdf&siren=' . $_GET['siren'] . '&cloture=' . intval($compte_annuel['dat_cloture']) . '"><img alt="pdf" style="width:32px;filter:drop-shadow(2px 2px 2px rgba(80,80,80,.7))" src="/images/pdf.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a></div>';
					echo '<div style="display:inline-block;margin:5px"><a href="' . '/download.php?type=bilan&filetype=xml&siren=' . $_GET['siren'] . '&cloture=' . intval($compte_annuel['dat_cloture']) . '"><img alt="xml" style="width:32px;filter:drop-shadow(2px 2px 2px rgba(80,80,80,.7))" src="/images/xml.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a></div>';
				}
				else if($compte_annuel['confidentialite'] == 'confidentialité totale')
					echo '<img alt="lock" class="icon" src="/images/lock.svg" onclick="alert(\'Ce document n\\\'est pas disponible au public.\nLa société a opté pour la confidentialité.\')"/>';
				else
					echo '<img alt="unavailable" class="icon" src="/images/unavailable.svg" onclick="alert(\'Les bilans déposés avant le 01/01/2017 ne sont pas disponibles\')"/>';
				echo '</td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}

//ETABLISSEMENTS
if (sizeof($etablissements) > 0)
{
	foreach ($etablissements as $key => $value)
    {
        $siege[$key]  = $value['siege'];
        $ouvert[$key] = ($value['fermeture']==''?1:0);
		$creation[$key] = strtotime($value['creation']);
		$fermeture[$key] = strtotime($value['fermeture']);
    }
	
    array_multisort($siege, SORT_DESC, $ouvert, SORT_DESC, $creation, SORT_DESC, $fermeture, SORT_DESC, $etablissements);
	
	echo '<table id="menu_etablissements" class="responsive_table etablissements scrollable_table">';
		echo '<thead>';
			echo '<tr><td>ETABLISSEMENTS (' . sizeof($etablissements) . ')</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($etablissements as $etablissement)
		{
			echo '<tr id="' . $etablissement['siret'] . '" style="min-height:70px">';
				echo '<td style="white-space:nowrap">' . ($etablissement['enseigne'][0]!=''?implode('<br/>',array_filter(array_map('trim', $etablissement['enseigne']))).'<br/>':'') . ($etablissement['siret']?$etablissement['siret']:'') . ($etablissement['siege']?'<br/>(siège)':'<br/>(secondaire)') . '</td>';
				echo '<td>' . ($etablissement['ape']?$etablissement['ape']:'') . '</td>';
				echo '<td style="text-align:center">' . ($etablissement['creation']?'<span class="smartphone_hide">Création<br/></span>'.date('d/m/Y',strtotime($etablissement['creation'])):'') . '</td>';
				echo '<td style="text-align:center">' . ($etablissement['fermeture']?'<span class="smartphone_hide">Fermeture<br/></span>'.date('d/m/Y',strtotime($etablissement['fermeture'])):'') . '</td>';
				echo '<td style="width:100%;overflow-wrap:anywhere">' . implode('<br/>',array_filter(array_map('trim', $etablissement['adresse']))) . '</td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}

//ACCORDS D'ENTREPRISE
if (is_array($etablissements_avec_accords))
{
	ksort($etablissements_avec_accords);
	echo '<table id="menu_accords" class="responsive_table scrollable_table accords_entreprise">';
		echo '<thead>';
			echo '<tr><td colspan="7">ACCORDS D\'ENTREPRISE</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($etablissements_avec_accords as $siret => $etablissement_avec_accords)
			foreach($etablissement_avec_accords['accords'] as $accord)
			{
				echo '<tr>';
					if ($siret != $lastsiret)
						echo '<td rowspan="' . sizeof($etablissement_avec_accords['accords']) . '" onclick="document.getElementById(\'' . $siret . '\').scrollIntoView({behavior:\'smooth\', block:\'start\', inline:\'nearest\'})"><a href="javascript:return false;">' . $siret . '</a></td>';
					else
						echo '<td align="center" class="smartphone_only" onclick="document.getElementById(\'' . $siret . '\').scrollIntoView({behavior: \'smooth\', block:\'start\', inline:\'nearest\'})"><a href="javascript:return false;">' . $siret . '</a></td>';
					echo '<td style="text-align:center"><span class="smartphone_hide">Signature<br/></span>' . ($accord['signature']?date('d/m/Y',strtotime($accord['signature'])):'') . '</td>';
					echo '<td style="text-align:center"><span class="smartphone_hide">Diffusion<br/></span>' . ($accord['diffusion']?date('d/m/Y',strtotime($accord['diffusion'])):'') . '</td>';
					echo '<td style="width:70%">' . ($accord['titre']?$accord['titre']:'') . '</td>';
					echo '<td style="width:30%">' . ($accord['themes']?implode('<br/>',$accord['themes']):'') . '</td>';
					echo '<td class="print_hide" style="text-align:center"><a href="' . '/download_acco.php?id=' . $accord['id'] . '"><img alt="doc" class="icon" src="/images/doc.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a></td>';
				echo '</tr>';
				$lastsiret = $siret;
			}
		echo '</tbody>';
	echo '</table>';
}

//MARQUES
if (is_array($marques))
{
	krsort($marques);
	echo '<table id="menu_marques" class="responsive_table marques scrollable_table">';
		echo '<thead>';
			echo '<tr><td colspan="5">MARQUES DEPOSEES (' . sizeof($marques) . ')</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($marques as $marque)
		{
			echo '<tr>';
				echo '<td style="text-align:center">' . $marque['office'] . ' ' . $marque['numero'] . '</td>';
				echo '<td style="text-align:center;width:70%;overflow-wrap:anywhere">' . $marque['nom'] . ($marque['type']!='Word'?'<br/><img src="https://data.inpi.fr/image/marques/' . $marque['office'] . $marque['numero'] . '" alt="marque" style="max-height:150px;max-width:300px" onerror="this.onerror=null; this.remove();" />':'') . '</td>';
				echo '<td style="text-align:center;width:30%;overflow-wrap:anywhere">' . $marque['classes'] . '</td>';
				echo '<td style="text-align:center">' . $marque['depot'] . '</td>';
				echo '<td style="text-align:center">' . $marque['expiration'] . '</td>';
				echo '<td><a href="https://bases-marques.inpi.fr/Typo3_INPI_Marques/marques_fiche_resultats.html?id=' . $marque['office'] . $marque['numero'] . '" target="_blank"><img alt="html" class="icon" src="/images/html.svg" onclick="document.getElementById(\'trademarks_form\').submit()"/></a></td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}

//BREVETS
if (is_array($brevets))
{
	krsort($brevets);
	echo '<table id="menu_brevets" class="responsive_table brevets scrollable_table">';
		echo '<thead>';
			echo '<tr><td colspan="5">BREVETS DEPOSES (' . sizeof($brevets) . ')</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($brevets as $brevet)
		{
			echo '<tr>';
				echo '<td style="text-align:center">' . $brevet['office'] . $brevet['numero'] . '</td>';
				echo '<td style="text-align:left;width:100%;overflow-wrap:anywhere">' . $brevet['nom']  . '</td>';
				echo '<td style="text-align:center">' . $brevet['depot'] . '</td>';
				echo '<td style="text-align:center">' . $brevet['expiration'] . '</td>';
				echo '<td><a href="https://worldwide.espacenet.com/publicationDetails/biblio?FT=D&CC=' . $brevet['office'] . '&NR=' . $brevet['numero'] . $brevet['reference']. '" target="_blank"><img alt="json" class="icon" src="/images/html.svg"/></a></td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}

//DESSINS & MODELES
if (is_array($designs))
{
	krsort($designs);
	echo '<table id="menu_designs" class="responsive_table designs scrollable_table">';
		echo '<thead>';
			echo '<tr><td colspan="5">DESSINS ET MODELES DEPOSES (' . sizeof($designs) . ')</td></tr>';
		echo '</thead>';
		echo '<tbody>';
		foreach($designs as $design)
		{
			echo '<tr>';
				echo '<td style="text-align:center">' . $design['office'] . $design['numero'] . '</td>';
				echo '<td style="text-align:center;width:100%;overflow-wrap:anywhere">' . $design['nom']  . '<br/><img src="https://data.inpi.fr/image/dessins_modeles/' . $design['office'] . $design['numero'] . '?ref=001" alt="design" style="max-height:150px" onerror="this.onerror=null; this.remove();" /></td>';
				echo '<td style="text-align:center">' . $design['depot'] . '</td>';
				echo '<td style="text-align:center">' . $design['expiration'] . '</td>';
				echo '<td><a href="https://bases-modeles.inpi.fr/Typo3_INPI_Modeles/dessins_fiche_resultats.html?id=' . $design['office'] . $design['numero'] . '_001" target="_blank"><img alt="json" class="icon" src="/images/html.svg"/></a></td>';
			echo '</tr>';
		}
		echo '</tbody>';
	echo '</table>';
}

//SOURCES
echo '<table id="menu_sources" class="scrollable_table print_hide">';
	echo '<thead>';
		echo '<tr><td style="text-align:left">SOURCES</td></tr>';
	echo '</thead>';
	echo '<tbody>';
		echo '<tr><td width="100%"><a href="https://api.insee.fr" target="_blank">Répertoire SIRENE</a></td><td style="text-align:center" class="print_hide"><a href="/sirene/' . $_GET['siren'] . '_unitelegale.json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a>&nbsp;&nbsp;<a href="/sirene/' . $_GET['siren'] . '_etablissements.json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		echo '<tr><td><a href="https://data.inpi.fr" target="_blank">Répertoire RNCS</a></td><td style="text-align:center" class="print_hide"><a href="/imrs/' . $_GET['siren'] . '.xml" target="_blank"><img alt="xml" class="icon" src="/images/xml.svg"/></a>&nbsp;&nbsp;<a href="/imrs/' . $_GET['siren'] . '_actes.json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		if (is_array($beneficiaires_advanced))
			echo '<tr><td><a href="https://data.inpi.fr" target="_blank">Registre des Bénéficiaires Effectifs</a><br/><span style="font-size:12px;font-style:italic;color:#FF0000">Données confidentielles réservées aux avocats</span></td><td style="text-align:center" class="print_hide"><a href="/rbe/' . $_GET['siren'] . '.xml" target="_blank"><img alt="xml" class="icon" src="/images/xml.svg"/></a>&nbsp;&nbsp;<a href="/rbe/' . $_GET['siren'] . '_actes.json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		if (is_array($etablissements_avec_accords))
			echo '<tr><td><a href="https://aife.economie.gouv.fr/dila/legifrance-beta" target="_blank">LEGIFRANCE</a></td><td style="text-align:center" class="print_hide"><a href="/legifrance_acco/' . $_GET['siren'] . '_brut.json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		if (is_array($annonces))
			echo '<tr><td><a href="https://www.bodacc.fr" target="_blank">BODACC</a></td><td style="text-align:center;white-space:nowrap" class="print_hide"><a href="https://www.bodacc.fr/annonce/liste/' . $_GET['siren'] . '" target="_blank"><img alt="json" class="icon" src="/images/html.svg"/></a>&nbsp;&nbsp;<a href="/bodacc/' . $_GET['siren'] . '.json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		if (is_array($idcc))
			echo '<tr><td><a href="https://siret2idcc.fabrique.social.gouv.fr/" target="_blank">SIRET2IDCC</a></td><td style="text-align:center;white-space:nowrap" class="print_hide"><a href="/idcc/' . $_GET['siren'] . '.json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		if (is_array($rm))
			echo '<tr><td><a href="https://api-rnm.artisanat.fr/v2/entreprises/" target="_blank">API RNM</a></td><td style="text-align:center;white-space:nowrap" class="print_hide"><a href="/rm/' . $_GET['siren'] . '.json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		if (is_array($marques))
			echo '<tr><td><a href="https://data.inpi.fr" target="_blank">INPI.FR (Marques)</a></td><td style="text-align:center;white-space:nowrap" class="print_hide"><a href="https://societe.ninja/trademarks.php?siren=' . $_GET['siren'] . '&format=json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		if (is_array($brevets))
			echo '<tr><td><a href="https://data.inpi.fr" target="_blank">INPI.FR (Brevets)</a></td><td style="text-align:center;white-space:nowrap" class="print_hide"><a href="https://societe.ninja/patents.php?siren=' . $_GET['siren'] . '&format=json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
		if (is_array($designs))
			echo '<tr><td><a href="https://data.inpi.fr" target="_blank">INPI.FR (Dessins & Modèles)</a></td><td style="text-align:center;white-space:nowrap" class="print_hide"><a href="https://societe.ninja/designs.php?siren=' . $_GET['siren'] . '&format=json" target="_blank"><img alt="json" class="icon" src="/images/json.svg"/></a></td></tr>';
	echo '</tbody>';
echo '</table>';

echo '<br/><br/>';

//FOOTER
echo '<table class="footer">';
	echo '<tr>';
		echo '<td style="text-align:right" class="smartphone_hide"><a href="https://www.optimus-avocats.fr" target="_blank"><img alt="optimus" style="max-width:170px" src="https://prod.optimus-avocats.fr/images/logo/optimus-avocats.svg"/></a></td>';
		echo '<td style="text-align:center;width:80%">';
			echo 'Cette page est offerte par <a href="https://www.optimus-avocats.fr" target="_blank">OPTIMUS AVOCATS</a>.<br/>';
			echo 'Soutenez nos développeurs en faisant <a href="https://www.optimus-avocats.fr/donation.html" target="_blank">un don</a>.';
		echo '</td>';
		echo '<td style="text-align:right" class="smartphone_hide"><a href="https://www.optimus-avocats.fr" target="_blank"><img alt="optimus" style="max-width:170px" src="https://prod.optimus-avocats.fr/images/logo/optimus-avocats.svg"/></a></td>';
	echo '</tr>';
echo '</table>';

echo '<br/><br/>';

echo '</div>';

echo '<div id="home" style="position:fixed;top:0;left:0;z-index:3;" class="print_hide"><a href="index.php"><img alt="home" class="menu_icon" style="filter:invert(1)" src="/images/home.svg"/></a></div>';
echo '<div id="print" style="position:fixed;top:0;left:48px;z-index:3;" class="print_hide"><a href="javascript:window.print()"><img alt="printer" class="menu_icon" style="filter:invert(1)" src="/images/printer.svg"/></a></div>';
echo '<div id="refresh" style="position:fixed;top:0;right:48px;z-index:3;" class="print_hide"><a href="/data.php?siren=' . $_GET['siren'] . '&refresh=1"><img alt="printer" class="menu_icon" style="filter:invert(1)" src="/images/refresh.svg"/></a></div>';
echo '<div id="menu" style="position:fixed;top:0;right:0px;z-index:3;" class="smartphone_only print_hide" onclick="menu()"><img alt="home" class="menu_icon" style="filter:invert(1)" src="/images/bars.svg"/></div>';
?>

<script>
function menu()
{
	if (window.innerWidth <= 760)
		for (i=0; i<header_links.children.length; i++) 
			header_links.children[i].style.display = (header_links.children[i].style.display=='block'?'none':'block');
}
</script>

</body>
</html>
